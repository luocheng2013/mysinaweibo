//
//  DetailViewController.m
//  SinaWeiBo
//
//  Created by wangxin mao on 14-2-28.
//  Copyright (c) 2014年 wangxin mao. All rights reserved.
//

#import "DetailViewController.h"
#import "EGORefreshTableHeaderView.h"
#import "WeiboSDK.h"

@interface DetailViewController ()

@end


@implementation DetailViewController


@synthesize tempBtn;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {

    }
    return self;
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIButton *testBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [testBtn setTitle:@"测试Push" forState:UIControlStateNormal];
    testBtn.frame = CGRectMake(80, 200, 150, 38);
    UIImage *normalImage = [[UIImage imageNamed:@"common_button_big_green_os7"] stretchableImageWithLeftCapWidth:3 topCapHeight:3];
    [testBtn setBackgroundImage:normalImage forState:UIControlStateNormal];
    [testBtn addTarget:self action:@selector(detailVC:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:testBtn];

    
    UIButton *requestBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [requestBtn setTitle:@"返回" forState:UIControlStateNormal];
    requestBtn.frame = CGRectMake(80, 300, 150, 38);
    UIImage *normalImage2 = [[UIImage imageNamed:@"common_button_big_green_os7"] stretchableImageWithLeftCapWidth:3 topCapHeight:3];
    [requestBtn setBackgroundImage:normalImage2 forState:UIControlStateNormal];
    [requestBtn addTarget:self action:@selector(backAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:requestBtn];
    
}

- (void)detailVC:(UIButton *)sender
{
    DetailViewController *detailVC = [[DetailViewController alloc] initWithNibName:@"DetailViewController" bundle:nil];
    [self.navigationController pushViewController:detailVC animated:YES];
}

- (void)backAction:(UIButton *)sender
{
    if ([self.navigationController.viewControllers count] > 1) {
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        [self dismissViewControllerAnimated:YES completion:^{ }];
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
