//
//  HomeViewController.h
//  SinaWeiBo
//
//  Created by wangxin mao on 14-2-27.
//  Copyright (c) 2014年 wangxin mao. All rights reserved.
//

#import "BaseViewController.h"
#import "WeiboSDK.h"

@interface HomeViewController : BaseViewController<UITableViewDataSource,UITableViewDelegate,EGORefreshTableHeaderDelegate,WBHttpRequestDelegate,WeiboSDKDelegate>

@property (nonatomic, retain) UITableView *infoTableView;
@property (nonatomic, retain) EGORefreshTableHeaderView *refreshHeaderView;
@property (nonatomic, retain) UIView *refreshBar;
@property (nonatomic, retain) NSMutableArray *infoDataArr;
@property (nonatomic, retain) NSMutableArray *tempDataArr;
@property (nonatomic, retain) NSMutableDictionary *allCellDic;
@property (nonatomic, retain) NSString *wbtoken;
@property (nonatomic, retain) UIView *collectView;
@property (nonatomic, retain) UIView *functionView;
@property (nonatomic, assign) CGFloat cellContentHeight;

- (BOOL)handleOpenURLFromWeiBo:(NSURL *)url;

- (void)requestInfoData;

//走此方法收起下拉区
- (void)doneLoadingTableViewData;

- (void)getWantToDealWithData:(NSMutableArray *)getArr statuses_tag:(NSString *)requestTag;

- (void)scrollTableView;

- (void)collectActionFromCell;

@end
