//
//  HomeViewController.m
//  SinaWeiBo
//
//  Created by wangxin mao on 14-2-27.
//  Copyright (c) 2014年 wangxin mao. All rights reserved.
//

#import "HomeViewController.h"
#import "LoginViewController.h"
#import "SCGIFImageView.h"
#import "DetailViewController.h"
#import "InfoCell.h"
#import "WeiboSDK.h"


@interface HomeViewController ()
{
    BOOL _reloading;
    AppContextManagement *app;
    LoginViewController *loginVC;
    DealWithRequestData *dealWith;
}

@end

@implementation HomeViewController

@synthesize infoTableView;
@synthesize refreshHeaderView,infoDataArr,allCellDic,tempDataArr,wbtoken,collectView,functionView;
@synthesize cellContentHeight;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"主页";
    }
    return self;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.infoTableView setFrame:CGRectMake(0, 64, KscreenWidth, KscreenHeight-20-44-49)];
    /*
    //根据条件判断如果没登就弹出登陆页面
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    if (![userDefault objectForKey:@"WeiBo_Login_Token"]) {
        loginVC = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
        MLNavigationController *loginNav = [[MLNavigationController alloc] initWithRootViewController:loginVC];
        [self presentViewController:loginNav animated:NO completion:^{}];
    } */

    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    
    if (![userDefault objectForKey:@"WeiBo_Login_Token"]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示"
                                                        message:@"请点击右上角登录"
                                                       delegate:nil
                                              cancelButtonTitle:@"确定"
                                              otherButtonTitles:nil];
        [alert show];
    }

    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"ad_background@2x"]];
//    self.view.backgroundColor = [UIColor brownColor];
    app = [AppContextManagement shareAppContextmanagement];
    
    NSMutableArray *dataArr = [[NSMutableArray alloc] initWithCapacity:10];
    self.infoDataArr = dataArr;//表格cell数
    
    NSMutableArray *tempCellDataArr = [[NSMutableArray alloc] initWithCapacity:10];
    self.tempDataArr = tempCellDataArr;//每个cell数据
    
    NSMutableDictionary *weiboDataDic = [[NSMutableDictionary alloc] initWithCapacity:10];
    self.allCellDic = weiboDataDic;
    
    
    //展示左边
    UIImage *leftBtnImg = [UIImage imageNamed:@"navigationbar_friendsearch_os7"];
    UIButton *showLeftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [showLeftBtn setFrame:CGRectMake(0, 0, 30, 30)];
    [showLeftBtn setImage:leftBtnImg forState:0];
    [showLeftBtn addTarget:self action:@selector(showLeftVCAction:) forControlEvents:UIControlEventTouchUpInside];
    showLeftBtn.showsTouchWhenHighlighted = YES;
        UIBarButtonItem *showLeftBtnItem = [[UIBarButtonItem alloc] initWithCustomView:showLeftBtn];
    self.navigationItem.leftBarButtonItem = showLeftBtnItem;
    
    
    //右边按钮
    UIImage *rightBtnImg = [UIImage imageNamed:@"navigationbar_pop_os7"];
    UIButton *showRightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [showRightBtn setFrame:CGRectMake(0, 0, 30, 30)];
    [showRightBtn setImage:rightBtnImg forState:0];
    [showRightBtn addTarget:self action:@selector(showRightVCAction:) forControlEvents:UIControlEventTouchUpInside];
    showRightBtn.showsTouchWhenHighlighted = YES;
    UIBarButtonItem *showRightBtnItem = [[UIBarButtonItem alloc] initWithCustomView:showRightBtn];
    self.navigationItem.rightBarButtonItem = showRightBtnItem;

    
    UITableView *infoTab = [[UITableView alloc] initWithFrame:CGRectMake(0, 64, KscreenWidth, KscreenHeight-20-44) style:UITableViewStyleGrouped];
    infoTab.backgroundColor = [UIColor clearColor];
    infoTab.separatorStyle = UITableViewCellSeparatorStyleNone;
    infoTab.delegate = self;
    infoTab.dataSource = self;
    [self.view addSubview:infoTab];
    self.infoTableView = infoTab;
    
    
    EGORefreshTableHeaderView *customRefreshHeaderView =[[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f,-40, self.infoTableView.frame.size.width,40)];
    customRefreshHeaderView.delegate = self;
    self.refreshHeaderView = customRefreshHeaderView;
    [infoTab addSubview:customRefreshHeaderView];
    
    //初始化全局app属性。(警告：此方法只能等整个App框架显示出来后才能调用，否则有空对象)
    [self createAppContextData];
    
    //cell右上角下拉收藏等功能View
    [self createCollectFunctionView];
    
    
    //请求数据类
    dealWith = [[DealWithRequestData alloc] initWithRequestTag:@"statuses/home_timeline"];
    
    //请求数据
    [self requestInfoData];
    
    //请求用户名
    [self requestUesrName];
}


- (void)createAppContextData
{
    //此方法为创建一个全局的数组，意指在tabar的四个类中（除新建类以外）的viewWill方法中创建一张截屏，方面在新建的磨砂背景下显示
    [app createscreenShotArr];
    
    //此方法在创建tabbar的viewWill方法中调用，以后在每个类中用app.的形式就可以得到每个tabbar的对象了
    [app createSinaWeiBoAllVC];
}


- (void)createCollectFunctionView
{
    //灰色透明蒙层
    UIView *myCollectView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, KscreenWidth, KscreenHeight)];
    myCollectView.backgroundColor = [UIColor colorWithWhite:0.0 alpha:.4];
    myCollectView.alpha = 0.0;
    self.collectView = myCollectView;
    [self.tabBarController.view addSubview:myCollectView];
    
    
    //功能按钮
    UIView *myFunctionView = [[UIView alloc] initWithFrame:CGRectMake(0, KscreenHeight, KscreenWidth, KscreenHeight/3+20)];
    myFunctionView.backgroundColor = [UIColor colorWithWhite:0.0 alpha:.4];
    self.functionView = myFunctionView;
    [myCollectView addSubview:myFunctionView];
    
    
    
    NSArray *titleArr = [NSArray arrayWithObjects:@"收藏",@"屏蔽Ta的微博",@"取消关注",@"举报", nil];
    
    for (int i = 0 ; i < [titleArr count]; i++) {
        UIButton *functionBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        functionBtn.frame = CGRectMake(0, 40*i-1, KscreenWidth, 41);
        functionBtn.tag = i;
        [functionBtn setTitle:titleArr[i] forState:0];
        [functionBtn setTitleColor:[UIColor blackColor] forState:0];
        [functionBtn setBackgroundImage:[UIImage imageNamed:@"more_Individuation_toolbar"] forState:0];
        [functionBtn addTarget:self action:@selector(functionBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [myFunctionView addSubview:functionBtn];
    }
    
    UIButton *cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [cancelBtn setBackgroundImage:[UIImage imageNamed:@"more_Individuation_toolbar"] forState:0];
    [cancelBtn setTitle:@"取消" forState:0];
    [cancelBtn setTitleColor:[UIColor blackColor] forState:0];
    cancelBtn.frame = CGRectMake(0, 40*4+5, KscreenWidth, 40);
    [cancelBtn addTarget:self action:@selector(cancelBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [myFunctionView addSubview:cancelBtn];

    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dissmissCollectView:)];
    [myCollectView addGestureRecognizer:tapRecognizer];
}

//下拉菜单功能
- (void)functionBtnAction:(UIButton *)sender
{
    switch (sender.tag) {
        case 0:
            //收藏逻辑
            break;
        case 1:
            //屏蔽ta的微博逻辑
            break;
        case 2:
            //取消关注逻辑
            break;
        case 3:
            //举报逻辑
            break;
            
        default:
            break;
    }
    [self dissmissCollectView:nil];
}

//取消
- (void)cancelBtnAction:(id)sender
{
    [self dissmissCollectView:nil];
}


- (void)collectActionFromCell
{
    [UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:0.4];
    self.collectView.alpha = 1.0;
	[self.functionView setFrame:CGRectMake(0, KscreenHeight-KscreenHeight/3-15, KscreenWidth, KscreenHeight/3+20)];
	[UIView commitAnimations];
}


- (void)dissmissCollectView:(UITapGestureRecognizer *)recognizer
{
    DLog(@"dissmissCollectView");
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDelegate:self];
	[UIView setAnimationDuration:0.4];
    self.collectView.alpha = 0.0;
	[self.functionView setFrame:CGRectMake(0, KscreenHeight, KscreenWidth, KscreenHeight/3+20)];
	[UIView commitAnimations];
}


- (CGFloat)gettextHright:(NSInteger)whichSection
{
    
    NSDictionary *receiveDic = [self.infoDataArr objectAtIndex:whichSection];
    
    NSString *weiboContenText = [receiveDic objectForKey:@"text"];
    
    CGFloat textHeight = 0.0;
    if (weiboContenText) {
        
        NSDictionary *textDic = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:14], NSFontAttributeName, nil];
        CGSize textSize = [weiboContenText boundingRectWithSize:CGSizeMake(305.0, CGFLOAT_MAX)                                                     options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading
                                                     attributes:textDic
                                                        context:nil].size;
        
        textHeight = textSize.height;
//        DLog(@"根据获取数据的多少来控制cell高度===%.f",textHeight);
        
    } else {
        textHeight = 0.0;
    }
    
    return textHeight;
}

//获取用户名
- (void)requestUesrName
{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    
    if ([userDefault objectForKey:@"WeiBo_Login_Token"]) {
        
        NSMutableDictionary *paramsText = [[NSMutableDictionary alloc] initWithCapacity:10];
        [paramsText setObject:kAppKey forKey:@"source"];
        [paramsText setObject:[userDefault objectForKey:@"WeiBo_Login_Token"] forKey:@"access_token"];
        
        if ([userDefault objectForKey:@"userUid"]) {
            [paramsText setObject:[userDefault objectForKey:@"userUid"] forKey:@"uid"];
        }
        
        [WBHttpRequest requestWithURL:@"https://api.weibo.com/2/users/show.json" httpMethod:@"GET" params:paramsText delegate:dealWith withTag:@"users/show"];
    }
}

#pragma mark - 表格的代理方法
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.infoDataArr count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return 8;
    } else {
        return 4;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 4;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    根据获取数据的多少来控制cell高度;内容以前的固定高度是60;
    CGFloat textHeight = [self gettextHright:indexPath.section];
    CGFloat picHeight = 0.0;
    
    NSDictionary *receiveDic = [self.infoDataArr objectAtIndex:indexPath.section];
    if ([receiveDic objectForKey:@"thumbnail_pic"]) {
        picHeight = 80.0;
    } else {
        picHeight = 0.0;
    }

    CGFloat allHight = 90.0 + 5.0 + textHeight + 10.0 + picHeight + 50.0;
//    self.cellContentHeight = allHight;
    
    return allHight;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    NSString *cellIdfaine = @"customCell";
    
    InfoCell *infoCell = [tableView dequeueReusableCellWithIdentifier:cellIdfaine];

    if (infoCell == nil) {
        infoCell = [[InfoCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdfaine];
    }
    [infoCell setDataToCell:[self.infoDataArr objectAtIndex:indexPath.section] contenTextHeight:[self gettextHright:indexPath.section] contentHeight:[self tableView:self.infoTableView heightForRowAtIndexPath:indexPath]];
    
    return  infoCell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.infoTableView setFrame:CGRectMake(0, 64, KscreenWidth, KscreenHeight-20-44)];
    DetailViewController *detailVC = [[DetailViewController alloc] initWithNibName:@"DetailViewController" bundle:nil];
    [self.navigationController pushViewController:detailVC animated:YES];
}


//多次按了第一个tabbar后请求数据
- (void)scrollTableView
{
    [UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:0.1];
	[self.infoTableView setContentInset:UIEdgeInsetsMake(70.0f, 0.0f, 0.0f, 0.0f)];
    [self.infoTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
	[UIView commitAnimations];
    [self.refreshHeaderView customRefresh:self.infoTableView];
}



#pragma mark - 下拉刷新模块
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.refreshHeaderView egoRefreshScrollViewDidScroll:scrollView];
}


- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    [self.refreshHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
}


- (BOOL)egoRefreshTableHeaderDataSourceIsLoading:(EGORefreshTableHeaderView*)view
{
    return _reloading;
}


- (NSDate*)egoRefreshTableHeaderDataSourceLastUpdated:(EGORefreshTableHeaderView*)view
{
    return [NSDate date];
}


- (void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView*)view
{
    DLog(@"下拉触发同步方法");
    
    if ([Common isInternetConnection]) {
        //下拉请求数据走这里
        [self requestInfoData];
        _reloading = YES;

    } else{
        [self performSelector:@selector(doneLoadingTableViewData) withObject:nil afterDelay:2.0];
        [Common showCustomerToastView:@"网络连接失败\n请检查网络"];
    }
}


#pragma mark - requestData 
- (void)requestInfoData
{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    
    if ([userDefault objectForKey:@"WeiBo_Login_Token"]) {
        
        NSMutableDictionary *paramsText = [[NSMutableDictionary alloc] initWithCapacity:10];
        [paramsText setObject:kAppKey forKey:@"source"];
        [paramsText setObject:[userDefault objectForKey:@"WeiBo_Login_Token"] forKey:@"access_token"];
        [paramsText setObject:@"100" forKey:@"count"];
        
        [WBHttpRequest requestWithURL:@"https://api.weibo.com/2/statuses/home_timeline.json" httpMethod:@"GET" params:paramsText delegate:dealWith withTag:@"statuses/home_timeline"];
        
    } else {
        [self performSelector:@selector(doneLoadingTableViewData) withObject:nil afterDelay:2.0];
        [Common showAlertString:@"请点击右上角登陆授权"];
    }
}



//统一处理每个请求的回调
- (void)getWantToDealWithData:(NSMutableArray *)getArr statuses_tag:(NSString *)requestTag
{
    if ([requestTag isEqualToString:@"statuses/home_timeline"]) {
        [self.infoDataArr removeAllObjects];
        [self.infoDataArr addObjectsFromArray:getArr];
        [self.infoTableView reloadData];
        [Common createMainScrenShot:self.navigationController.view];
        [self requestDataSuccess];
    }
    else if ([requestTag isEqualToString:@"users/show"]){
        if ([getArr count] >0) {
            NSDictionary *userNameDic = getArr[0];
            if ([userNameDic objectForKey:@"screen_name"]) {
                self.title = [userNameDic objectForKey:@"screen_name"];
            }
        }
    }
    else if ([requestTag isEqualToString:@""]){
        
    }
    else if ([requestTag isEqualToString:@""]){
        
    }

}


#pragma mark - refresh UI

//这个方法运行于子线程中，完成获取刷新数据的操作
-(void)requestDataSuccess
{
    DLog(@"doInBackground");
    //更新时间
    [self.refreshHeaderView refreshLastUpdatedDate:nil];
    
    //后台操作线程执行完后，到主线程更新UI
    [self performSelector:@selector(doneLoadingTableViewData) withObject:nil afterDelay:1.0];
}

//走此方法收起下拉区
- (void)doneLoadingTableViewData
{
    _reloading = NO;
    [self.refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:self.infoTableView];
}


#pragma mark -show left and right VC

- (void)showLeftVCAction:(id)sender
{
    [app.weiboDDMenuVC showLeftController:YES];
}

- (void)showRightVCAction:(id)sender
{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    
    if (![userDefault objectForKey:@"WeiBo_Login_Token"]) {
        //授权登录
        WBAuthorizeRequest *request = [WBAuthorizeRequest request];
        request.redirectURI = kRedirectURI;
        request.scope = @"all";
        request.userInfo = @{@"SSO_From": @"LoginViewController",
                             @"Other_Info_1": [NSNumber numberWithInt:123],
                             @"Other_Info_2": @[@"obj1", @"obj2"],
                             @"Other_Info_3": @{@"key1": @"obj1", @"key2": @"obj2"}};
        [WeiboSDK sendRequest:request];

    } else {
        
        [app.weiboDDMenuVC showRightController:YES];
    }
}

#pragma mark - SinaWeibo delegate method
- (void)didReceiveWeiboRequest:(WBBaseRequest *)request
{
    if ([request isKindOfClass:WBProvideMessageForWeiboRequest.class]) {
        DLog(@"didReceiveWeiboRequest＝＝＝接收请求");
    }
}

- (void)didReceiveWeiboResponse:(WBBaseResponse *)response
{
    if ([response isKindOfClass:WBSendMessageToWeiboResponse.class]) {
        NSString *title = @"发送结果";
        NSString *message = [NSString stringWithFormat:@"响应状态: %d\n响应UserInfo数据: %@\n原请求UserInfo数据: %@",(int)response.statusCode, response.userInfo, response.requestUserInfo];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                        message:message
                                                       delegate:nil
                                              cancelButtonTitle:@"确定"
                                              otherButtonTitles:nil];
        [alert show];
        
    } else if ([response isKindOfClass:WBAuthorizeResponse.class]) {
        
        NSString *message = [NSString stringWithFormat:@"响应状态: %d\nresponse.userId: %@\nresponse.accessToken: %@\n响应UserInfo数据: %@\n原请求UserInfo数据: %@",(int)response.statusCode,[(WBAuthorizeResponse *)response userID], [(WBAuthorizeResponse *)response accessToken], response.userInfo, response.requestUserInfo];
        
        DLog(@"认证结果回调的信息=======%@",message);
        
        self.wbtoken = [(WBAuthorizeResponse *)response accessToken];
        
        DLog(@"登录获取的token======%@",self.wbtoken);
    }
    
    NSString *tipStr = nil;
    if (response.statusCode == 0) {
        
        NSString *userUid = [response.requestUserInfo objectForKey:@"uid"];
        tipStr = @"登录成功";
        [[NSUserDefaults standardUserDefaults] setObject:self.wbtoken forKey:@"WeiBo_Login_Token"];
        [[NSUserDefaults standardUserDefaults] setObject:userUid forKey:@"userUid"];
        
        [[NSUserDefaults standardUserDefaults] synchronize];

        
    } else {
        tipStr = @"登录失败";
    }
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"认证结果"
                                                    message:tipStr
                                                   delegate:nil
                                          cancelButtonTitle:@"确定"
                                          otherButtonTitles:nil];
    [alert show];
    [self requestInfoData];//登录完成后请求数据
}


#pragma mark - login delegate
//微博登录授权回调url
- (BOOL)handleOpenURLFromWeiBo:(NSURL *)url
{
    return [WeiboSDK handleOpenURL:url delegate:self];
}




@end
