//
//  SenderWeiboViewController.h
//  SinaWeiBo
//
//  Created by maowangxin on 14-3-20.
//  Copyright (c) 2014年 wangxin mao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SenderWeiboViewController : UIViewController<UITextViewDelegate>


@property (nonatomic, retain) NSString *weiboAppendingStr;

@property (nonatomic, retain) NSString *senderType;


@end
