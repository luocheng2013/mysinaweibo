//
//  SenderWeiboViewController.m
//  SinaWeiBo
//
//  Created by maowangxin on 14-3-20.
//  Copyright (c) 2014年 wangxin mao. All rights reserved.
//

#import "SenderWeiboViewController.h"
#import "PlaceTextView.h"
#import "DealWithRequestData.h"

@interface SenderWeiboViewController ()<UINavigationControllerDelegate,UIImagePickerControllerDelegate>
{

    PlaceTextView  * suggestTextView;
    DealWithRequestData *dealWith;
//    UIImagePickerController *imagePickerViewController;

}
@end

@implementation SenderWeiboViewController


@synthesize weiboAppendingStr,senderType;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"发微博";
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if ([self.senderType isEqualToString:@"snederPhotoAction"]) {
        self.senderType = @"snederPhotoAction2";
        [self senderPhotoAction];
    }
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //取消
    UIButton *cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [cancelBtn setFrame:CGRectMake(0, 0, 40, 30)];
    [cancelBtn setTitle:@"取消" forState:0];
    [cancelBtn setTitleColor:[UIColor orangeColor] forState:0];
    [cancelBtn addTarget:self action:@selector(cancelAction:) forControlEvents:UIControlEventTouchUpInside];
    cancelBtn.showsTouchWhenHighlighted = YES;
    UIBarButtonItem *showLeftBtnItem = [[UIBarButtonItem alloc] initWithCustomView:cancelBtn];
    self.navigationItem.leftBarButtonItem = showLeftBtnItem;
    
    
    //发送
    UIButton *showRightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [showRightBtn setFrame:CGRectMake(0, 0, 40, 30)];
    [showRightBtn setTitle:@"发送" forState:0];
    [showRightBtn setTitleColor:[UIColor grayColor] forState:0];
    [showRightBtn addTarget:self action:@selector(senderWeiboAction:) forControlEvents:UIControlEventTouchUpInside];
    showRightBtn.showsTouchWhenHighlighted = YES;
    UIBarButtonItem *showRightBtnItem = [[UIBarButtonItem alloc] initWithCustomView:showRightBtn];
    self.navigationItem.rightBarButtonItem = showRightBtnItem;

    
    
    suggestTextView = [[PlaceTextView alloc] initWithFrame:CGRectMake(5, 0, 296, 94+60)];
    suggestTextView.tag = 600;
    suggestTextView.delegate = self;
    suggestTextView.text = @"";
    suggestTextView.font = [UIFont systemFontOfSize:15];
    suggestTextView.placeholder = @"分享新鲜事(内容不超过140个汉字)...";
    [suggestTextView setBackgroundColor:[UIColor clearColor]];
    [suggestTextView becomeFirstResponder];
    [self.view addSubview:suggestTextView];
    
    
    //请求数据类
    dealWith = [[DealWithRequestData alloc] initWithRequestTag:@"update.json"];
}

-(void)textViewDidBeginEditing:(UITextView *)textView
{
    
}


- (void)cancelAction:(id)sender
{
    if ([self.senderType isEqualToString:@"snederPhotoAction2"]) {
        [self.navigationController popToRootViewControllerAnimated:YES];
    } else {
        [self dismissViewControllerAnimated:YES completion:^{ }];//退出页面
    }
}


- (void)senderWeiboAction:(id)sender
{
    if (suggestTextView.text.length > 140) {
        UIAlertView *tipAlert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"内容不超过140个汉字！" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [tipAlert show];
        return;
    } else if (suggestTextView.text.length == 0) {
        UIAlertView *tipAlert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"微博内容不能为空！" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [tipAlert show];
        return;
    }
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    
    if ([userDefault objectForKey:@"WeiBo_Login_Token"]) {
        
        NSMutableDictionary *paramsText = [[NSMutableDictionary alloc] initWithCapacity:10];
        [paramsText setObject:kAppKey forKey:@"source"];
        [paramsText setObject:[userDefault objectForKey:@"WeiBo_Login_Token"] forKey:@"access_token"];
        [paramsText setObject:@"0" forKey:@"visible"];//微博的可见性，0：所有人能看，1：仅自己可见，2：密友可见，3：指定分组可见，默认为0。
        [paramsText setObject:suggestTextView.text forKey:@"status"];//要发布的微博文本内容，必须做URLencode，内容不超过140个汉字。
        
        
        NSString *wanTosenderStr = nil;
        if (self.weiboAppendingStr != nil && ![self.weiboAppendingStr isEqualToString:@""]) {
            wanTosenderStr = [suggestTextView.text stringByAppendingString:self.weiboAppendingStr];
        } else {
            wanTosenderStr = suggestTextView.text;
        }
        
        DLog(@"wanTosenderStr=====%@",wanTosenderStr);
        

        [WBHttpRequest requestWithURL:@"https://api.weibo.com/2/statuses/update.json" httpMethod:@"POST" params:paramsText delegate:dealWith withTag:@"update.json"];
        
        [self dismissViewControllerAnimated:YES completion:^{ }];//退出页面
        
    }

}


- (void)senderPhotoAction
{
    UIImagePickerController *imagePickerViewController = [[UIImagePickerController alloc] init];
    imagePickerViewController.delegate = self;
    imagePickerViewController.allowsEditing = NO;
    imagePickerViewController.view.userInteractionEnabled = YES;
    imagePickerViewController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self.navigationController presentViewController:imagePickerViewController animated:YES completion:nil];
}


-(void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated{
    if (KsystemNum >= 7.0)
    {
        //IOS7下选中图片时将状态栏字体颜色改为黑色
        [[UIApplication  sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    }
}
-(void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated{
    if (KsystemNum >= 7.0)
    {
        //IOS7下选中图片时将状态栏字体颜色改为黑色
        [[UIApplication  sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    if ([mediaType isEqualToString:@"public.image"]) {
        NSURL *urlFerence=[info objectForKey:UIImagePickerControllerReferenceURL];
        NSString *ferenceUrl=[[urlFerence absoluteString] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        self.weiboAppendingStr = ferenceUrl;
        
        if (self.weiboAppendingStr != nil) {
            DLog(@"self.weiboAppendingStr=====%@",self.weiboAppendingStr);
        }
        
        //获取相册选择的图片
        //        UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
        
    }
    
    [picker dismissViewControllerAnimated:YES completion:^{ }];
}


- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    //关闭图片选中页面时把导航栏标题颜色置为白色
//    [[UINavigationBar appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[NSValue valueWithCGSize:CGSizeMake(0, 0)],UITextAttributeTextShadowOffset,[UIColor whiteColor],UITextAttributeTextColor,[UIFont boldSystemFontOfSize:20],UITextAttributeFont, nil]];
//    [[UIApplication  sharedApplication] setStatusBarStyle:UIStatusBarStyleBlackOpaque];
    
    [picker dismissViewControllerAnimated:YES completion:^{ }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
