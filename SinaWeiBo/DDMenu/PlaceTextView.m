//
//  PlaceTextView.m
//  CMOP
//
//  Created by akria on 12-11-7.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "PlaceTextView.h"

@implementation PlaceTextView
@synthesize placeholder;
@synthesize placeholderColor;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {

        textHight = [@"h" sizeWithFont:self.font].height;
        self.placeholder = @"";
        self.placeholderColor = [UIColor lightGrayColor];
    	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textChanged:) name:UITextViewTextDidChangeNotification object:nil];
    }
    return self;
}




-(void)textChanged:(NSNotification*)notif {
    if ([self.placeholder length]==0)
    	return;
    if ([[self text] length]==0) {
        [[self viewWithTag:9999] setAlpha:1];
    } else {
    	[[self viewWithTag:9999] setAlpha:0.0];
    }
    
}

- (void)drawRect:(CGRect)rect {
    if ([self.placeholder length]>0) {
        NSInteger x = 8;
        if (KsystemNum >= 7.0)
        {
            x=5;
        }
    	UILabel *lab = [[UILabel alloc] initWithFrame:CGRectMake(x, 8, 0,  0)];
    	[lab setFont:self.font];
        [lab setBackgroundColor:[UIColor clearColor]];
    	[lab setTextColor:self.placeholderColor];
    	[lab setText:self.placeholder];
    	[lab setAlpha:0];
    	[lab setTag:9999];
    	[self addSubview:lab];
    	[lab sizeToFit];
    	[lab release];
    }
    if ([[self text] length]==0 && [self.placeholder length]>0) {
    	[[self viewWithTag:9999] setAlpha:1];
    }
    [super drawRect:rect];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [super dealloc];
}


@end
