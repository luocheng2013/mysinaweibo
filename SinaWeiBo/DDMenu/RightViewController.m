//
//  RightViewController.m
//  SinaWeiBo
//
//  Created by wangxin mao on 14-2-27.
//  Copyright (c) 2014年 wangxin mao. All rights reserved.
//

#import "RightViewController.h"

@interface RightViewController ()
{
    AppContextManagement *app;
}
@end

@implementation RightViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"右边视图";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor cyanColor];
    app = [AppContextManagement shareAppContextmanagement];
    
    UIButton *logoutBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [logoutBtn setTitle:@"showHome" forState:UIControlStateNormal];
    logoutBtn.frame = CGRectMake(80, 200, 150, 38);
    UIImage *normalImage = [[UIImage imageNamed:@"common_button_big_green_os7"] stretchableImageWithLeftCapWidth:3 topCapHeight:3];
    [logoutBtn setBackgroundImage:normalImage forState:UIControlStateNormal];
    [logoutBtn addTarget:self action:@selector(detailVC:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:logoutBtn];
}


- (void)detailVC:(UIButton *)sender
{
    [app.weiboDDMenuVC showRootController:YES];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
