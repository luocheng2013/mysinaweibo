//
//  MainViewController.m
//  SinaWeiBo
//  这个是主页面（用户看到的登陆进来看到的第一个页面）
//
//  Created by wangxin mao on 14-2-27.
//  Copyright (c) 2014年 wangxin mao. All rights reserved.
//

#import "MainViewController.h"
#import "SCGIFImageView.h"


@interface MainViewController ()
{
    UIImageView *_activityImageView;
    AppContextManagement *app;
}

@end

@implementation MainViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"主视图";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor grayColor];
    app = [AppContextManagement shareAppContextmanagement];
    
    //第三方类显示gif图片
    NSString* filePath = [[NSBundle mainBundle] pathForResource:@"showLeftImg.gif" ofType:nil];
    _activityImageView = [[SCGIFImageView alloc] initWithGIFFile:filePath];
    _activityImageView.frame = CGRectMake(130, 200, 40, 40);
    [self.view addSubview:_activityImageView];
    

    UIButton *showLeftBtn = [[UIButton alloc] initWithFrame:CGRectMake(5, 27, 60, 35)];
    showLeftBtn.showsTouchWhenHighlighted = YES;
    [showLeftBtn setTitleColor:[UIColor darkTextColor] forState:0];
    [showLeftBtn setTitle:@"左菜单" forState:0];
    [showLeftBtn addTarget:self action:@selector(showLeftVCAction:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *showLeftItem = [[UIBarButtonItem alloc] initWithCustomView:showLeftBtn];
    
    self.navigationItem.leftBarButtonItem = showLeftItem;
    
}

- (void)showLeftVCAction:(id)sender
{
    [app.weiboDDMenuVC showLeftController:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
