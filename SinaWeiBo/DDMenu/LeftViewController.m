//
//  LeftViewController.m
//  SinaWeiBo
//
//  Created by wangxin mao on 14-2-27.
//  Copyright (c) 2014年 wangxin mao. All rights reserved.
//

#import "LeftViewController.h"
#import "DetailViewController.h"
#import "MainViewController.h"

@interface LeftViewController ()
{
    AppContextManagement *app;
}
@end

@implementation LeftViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"左边视图";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor lightGrayColor];
    app = [AppContextManagement shareAppContextmanagement];
    
    UIButton *logoutBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [logoutBtn setTitle:@"新浪微博" forState:UIControlStateNormal];
    logoutBtn.frame = CGRectMake(80, 200, 150, 38);
    UIImage *normalImage = [[UIImage imageNamed:@"common_button_big_green_os7"] stretchableImageWithLeftCapWidth:3 topCapHeight:3];
    [logoutBtn setBackgroundImage:normalImage forState:UIControlStateNormal];
    [logoutBtn addTarget:self action:@selector(detailVC:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:logoutBtn];
    
    
    UIButton *otherBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [otherBtn setTitle:@"第二个应用" forState:UIControlStateNormal];
    otherBtn.frame = CGRectMake(80, 300, 150, 38);
    UIImage *normalImage2 = [[UIImage imageNamed:@"common_button_big_green_os7"] stretchableImageWithLeftCapWidth:3 topCapHeight:3];
    [otherBtn setBackgroundImage:normalImage2 forState:UIControlStateNormal];
    [otherBtn addTarget:self action:@selector(otherVC:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:otherBtn];
}


- (void)detailVC:(UIButton *)sender
{
    [app.weiboDDMenuVC setRootController:app.weiboTabbarVC animated:YES];
}


- (void)otherVC:(UIButton *)sender
{
    MainViewController *mainVC = [[MainViewController alloc] initWithNibName:@"MainViewController" bundle:nil];
    MLNavigationController *mainNav = [[MLNavigationController alloc] initWithRootViewController:mainVC];
    [app.weiboDDMenuVC setRootController:mainNav animated:YES];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
