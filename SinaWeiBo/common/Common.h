//
//  Common.h
//  SinaWeiBo
//
//  Created by maowangxin on 14-3-6.
//  Copyright (c) 2014年 wangxin mao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SCGIFImageView.h"

@interface Common : NSObject


//整个主屏幕的截图
+ (void)createMainScrenShot:(UIView *)screnView;

//播放指定音频文件
+ (void)playSystemSound:(NSString *)soundName;

//弹框
+ (void)showAlertString:(NSString *)message;

+ (void)tipAlert:(NSString *)title tipStr:(NSString *)string;

//自定义弹框
+ (void)showCustomerToastView:(NSString *)tipText;

//活动指示器gif图片
+(SCGIFImageView *)createActive:(UIView *)addView;

//更新了几条微博
+(void)displayRefreshBar:(UIView *)addView refreshNum:(NSString *)contentNum;

//判断网络 NO: 没有连接，YES:已连接
+ (BOOL)isInternetConnection;

//判断是网络类型。0为没网，1为WIFI , 2为3G；
+ (int)internetType;

//截取视频第一帧存储到本地
+ (NSString *)generateVideoThumb:(NSString *)videoPath contentName:(NSString *)contentName;




@end
