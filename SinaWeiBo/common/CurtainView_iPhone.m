//
//  CurtainView_iPhone.m
//  mCloud_iPhone
//
//  Created by user on 13-10-9.
//  Copyright (c) 2013年 epro. All rights reserved.
//

#import "CurtainView_iPhone.h"
#import "SlideEffectLabel.h"

@interface CurtainView_iPhone ()
{
    SlideEffectLabel      *m_effectLabel;
}

@property (nonatomic, retain)SlideEffectLabel      *effectLabel;

@end

@implementation CurtainView_iPhone
@synthesize effectLabel=m_effectLabel;

- (void)dealloc
{
    [m_effectLabel release];
    [super dealloc];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        UIImageView *backView = [[UIImageView alloc] initWithFrame:self.frame];
        [backView setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",(iPhone5?@"Default-568h":@"Default")]]];
        [self addSubview:backView];
        [backView release];
        
        [self initEffectLabel];
    }
    return self;
}

- (void)initEffectLabel
{
    if (!m_effectLabel)
    {
        SlideEffectLabel *tmpEffectLabel = [[SlideEffectLabel alloc] initWithFrame:CGRectMake(50, 320*KscreenHeight/480, 300, 160)];
        self.effectLabel = tmpEffectLabel;
        [tmpEffectLabel release];
    }
    m_effectLabel.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
    
    m_effectLabel.font = [UIFont boldSystemFontOfSize:28];
    m_effectLabel.text = @"瞬息万变 你我之间";
    
    m_effectLabel.textColor = [UIColor blueColor];
    m_effectLabel.effectColor = [UIColor purpleColor];
    [self addSubview:m_effectLabel];
    
    int64_t delayInSeconds = 1;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [m_effectLabel performEffectAnimation];
    });
}

@end
