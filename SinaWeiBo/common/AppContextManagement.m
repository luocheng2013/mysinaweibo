//
//  AppContextManagement.m
//  SinaWeiBo
//
//  Created by maowangxin on 14-3-6.
//  Copyright (c) 2014年 wangxin mao. All rights reserved.
//

#import "AppContextManagement.h"
#import "AppDelegate.h"

static AppContextManagement *appContextManagement;

@implementation AppContextManagement


@synthesize screenShotArr;
@synthesize lastSelectNum,endSelectNum,customRefreshBar;
//全局的相应tabBar的类
@synthesize weiboTabbarVC,weiboDDMenuVC,weiboHomeVC,weiboMessageVC,weiboNewVC,weiboPlazaVC,weiboUserVC;
@synthesize requestSuccess;


+ (id)shareAppContextmanagement
{
    if (appContextManagement == Nil) {
        
        appContextManagement = [[AppContextManagement alloc] init];
    }
    return appContextManagement;
}

//此方法为创建一个全局的数组，意指在tabar的四个类中（除新建类以外）的viewWill方法中创建一张截屏，方面在新建的磨砂背景下显示
- (NSMutableArray *)createscreenShotArr
{
    if (screenShotArr == nil)
	{
        screenShotArr = [[NSMutableArray alloc] initWithCapacity:10];
	}
    return screenShotArr;
}

//此方法在HomeVC类的viewdidLoad方法中调用，以后在每个类中用app.的形式就可以得到每个tabbar的对象了
- (void)createSinaWeiBoAllVC
{
    CustomTabBarVC *customTabBar = [(AppDelegate *)[[UIApplication sharedApplication] delegate] appTabBarVC];
    
//******************这两个类要等整个app框架显示出来后才能创建，否则就是空的对象*****************
    
    DDMenuController *ddMenuVC = (DDMenuController *)[(AppDelegate *)[[UIApplication sharedApplication] delegate] ddMenuVC];
    
    CustomTabBarVC *tabbarVC = (CustomTabBarVC *)[(AppDelegate *)[[UIApplication sharedApplication] delegate] appTabBarVC];
    
    if (!appContextManagement.weiboDDMenuVC) {
        appContextManagement.weiboTabbarVC = tabbarVC;
    }

    if (!appContextManagement.weiboDDMenuVC) {
        appContextManagement.weiboDDMenuVC = ddMenuVC;
    }
//******************这个要等整个app框架显示出来后才能创建，否则就是空的对象*******************
    
    if (!appContextManagement.weiboHomeVC) {
        appContextManagement.weiboHomeVC = customTabBar.appHomeVC;
    }
    if (!appContextManagement.weiboMessageVC) {
        appContextManagement.weiboMessageVC = customTabBar.appMessageVC;
    }
    if (!appContextManagement.weiboNewVC) {
        appContextManagement.weiboNewVC = customTabBar.appNewVC;
    }
    if (!appContextManagement.weiboPlazaVC) {
        appContextManagement.weiboPlazaVC = customTabBar.appPlazaVC;
    }
    if (!appContextManagement.weiboUserVC) {
        appContextManagement.weiboUserVC = customTabBar.appUserVC;
    }
}


- (UIView *)creatRefreshView:(NSString *)refreshNum
{
    if (customRefreshBar == nil)
    {
        customRefreshBar = [[UIView alloc] initWithFrame:CGRectMake(0, -64, KscreenWidth, 40)];
        customRefreshBar.backgroundColor = ColorFromRGB(245, 228, 193);
        customRefreshBar.layer.cornerRadius = 3.0;
    
        UILabel *refreshLab = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, KscreenWidth, 20)];
        refreshLab.backgroundColor = [UIColor clearColor];
        refreshLab.text = refreshNum;
        refreshLab.font = [UIFont systemFontOfSize:14];
        refreshLab.textAlignment = 1;
        [customRefreshBar addSubview:refreshLab];
    }
    return customRefreshBar;
}






//清除用户数据
-(void)clearUserData
{
    [appContextManagement.weiboHomeVC.infoDataArr removeAllObjects];
    appContextManagement.screenShotArr = nil;
    appContextManagement.lastSelectNum = 0;
    appContextManagement.endSelectNum = 0;
    appContextManagement.requestSuccess = 0;
    
}

@end
