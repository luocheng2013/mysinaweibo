//
//  Common.m
//  SinaWeiBo
//
//  Created by maowangxin on 14-3-6.
//  Copyright (c) 2014年 wangxin mao. All rights reserved.
//

#import "Common.h"
#import <QuartzCore/QuartzCore.h>
#import "MBProgressHUD.h"
#import "Reachability.h"


@implementation Common

//整个主屏幕的截图
+ (void)createMainScrenShot:(UIView *)screnView
{
    //orgView 整个主屏幕
//    UIView *orgView = [[[UIApplication sharedApplication] delegate] window];//包括tabBar

//    这种方式截图不清晰,但速度快
    UIGraphicsBeginImageContext(screnView.bounds.size);
    [screnView.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    //清晰的截图
//    UIGraphicsBeginImageContextWithOptions(screnView.bounds.size, NO, 0.0);
//    CGContextRef context = UIGraphicsGetCurrentContext();
//    [screnView.layer renderInContext:context];
//    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
    
//    img就是获取的截图，测试是否截取成功，调用下面的方法然后在相册查看
//    UIImageWriteToSavedPhotosAlbum(img,nil,nil,nil);
    
    AppContextManagement *app = [AppContextManagement shareAppContextmanagement];
    [app.screenShotArr removeAllObjects];
    [app.screenShotArr addObject:img];
}

//播放指定音频文件
+ (void)playSystemSound:(NSString *)soundName
{
    SystemSoundID soundId;
    //后面将使用soundId引用音频文件
    NSString *soundFile = [[NSBundle mainBundle]pathForResource:soundName
                                                         ofType:@"wav"];
    //通过NSBundle的 mainBundle方法返回一个NSBundle对象，该对象对应当前应用程序可执行二进制文件所属目录。pathForResource方法获取aaa.wav的真实路径。
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)[NSURL fileURLWithPath:soundFile], &soundId);
    //创建soundFile的SystemSoundID,(__bridge CFURLRef)是强制转换成CFURLRef对象，因为此处要将一个c语言的结构转换为Objective-c对象，所以必须加上__bridge
    AudioServicesPlaySystemSound(soundId);
    //播放音频　System Sound Services不能播放越过30秒的声音，且只支持wav
}

//弹框
+ (void)showAlertString:(NSString *)message
{
    UIWindow *viewWindow = [[[UIApplication sharedApplication] windows] lastObject];
    MBProgressHUD *alert = [[MBProgressHUD alloc] initWithWindow:viewWindow];
    [viewWindow addSubview:alert];
    alert.mode = MBProgressHUDModeText;
    alert.detailsLabelText = message;
    alert.detailsLabelFont = [UIFont systemFontOfSize:16];
	[alert show:YES];
	[alert hide:YES afterDelay:2];
}

+ (void)tipAlert:(NSString *)title tipStr:(NSString *)string
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                       message:string
                                      delegate:nil
                             cancelButtonTitle:@"确定"
                             otherButtonTitles:nil];
    [alert show];
}

//自定义弹框
+ (void)showCustomerToastView:(NSString *)tipText
{
    UIWindow *viewWindow = [[[UIApplication sharedApplication] windows] lastObject];
    MBProgressHUD *alert = [[MBProgressHUD alloc] initWithWindow:viewWindow];
    [viewWindow addSubview:alert];
    alert.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"alert_error_icon"]];
    alert.mode = MBProgressHUDModeCustomView;
    alert.detailsLabelText = tipText;
    alert.detailsLabelFont = [UIFont systemFontOfSize:16];
	[alert show:YES];
	[alert hide:YES afterDelay:2];
}

//活动指示器gif图片
+(SCGIFImageView *)createActive:(UIView *)addView
{
    NSString* filePath = [[NSBundle mainBundle] pathForResource:@"showLeftImg.gif" ofType:nil];
    SCGIFImageView *activityImageView = [[SCGIFImageView alloc] initWithGIFFile:filePath];
    activityImageView.frame = CGRectMake(30, -5.0f, 20, 20);
    [activityImageView setHidden:YES];
    [addView addSubview:activityImageView];
    return activityImageView;
}

//每次更新了几条微博
+(void)displayRefreshBar:(UIView *)addView refreshNum:(NSString *)contentNum
{
    AppContextManagement *app = [AppContextManagement shareAppContextmanagement];
    UIView *refreshBar = [app creatRefreshView:contentNum];
    [addView addSubview:refreshBar];
    
    [UIView animateWithDuration:1.0 animations:^{
        refreshBar.hidden = NO;
        [refreshBar setFrame:CGRectMake(0, 0, KscreenWidth, 40)];
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:4.0 animations:^{
            [refreshBar setFrame:CGRectMake(0, -40, KscreenWidth, 40)];
            
        } completion:^(BOOL finished) {
            refreshBar.hidden = YES;
        }];
    }];
}

    //检测包含有特殊字符
+ (BOOL)isIncludeSpecialCharact: (NSString *)str
{
    NSRange urgentRange = [str rangeOfCharacterFromSet: [NSCharacterSet characterSetWithCharactersInString: @"\\/:*?\"<>|"]];
    if (urgentRange.location == NSNotFound)
        {
        return NO;
        }
    return YES;
}

//判断网络 NO: 没有连接，YES:已连接
+ (BOOL)isInternetConnection
{
    if ([Reachability reachabilityForInternetConnection] == nil || [[Reachability reachabilityForInternetConnection] currentReachabilityStatus] == 0)
        {
        return NO;
        }
    return YES;
}

//判断是网络类型。0为没网，1为WIFI , 2为3G；
+ (int)internetType
{
    Reachability *reach = [Reachability reachabilityForInternetConnection];
    NetworkStatus netStatus = [reach currentReachabilityStatus];
    
    switch (netStatus) {
        case NotReachable:
            return 0;
            break;
        case ReachableViaWWAN:
            return 1;
            break;
        case ReachableViaWiFi:
            return 2;
            break;
            
        default:
            break;
    }
}


//截取视频第一帧存储到本地
+ (NSString *)generateVideoThumb:(NSString *)videoPath contentName:(NSString *)contentName {
    if ([videoPath length] < 1 || [videoPath isEqualToString:@"(null)"]) {
        return nil;
    }
//    NSURL *videoURL = [NSURL fileURLWithPath:videoPath];
//    NSDictionary *opts = [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:NO] forKey:AVURLAssetPreferPreciseDurationAndTimingKey];
//    AVURLAsset *urlAsset = [AVURLAsset URLAssetWithURL:videoURL options:opts];
//    AVAssetImageGenerator *generator = [AVAssetImageGenerator assetImageGeneratorWithAsset:urlAsset];
//    generator.appliesPreferredTrackTransform = YES;
//    NSError *error = nil;
//    NSString *filePathThumbnail = nil;
//    CGImageRef img = [generator copyCGImageAtTime:CMTimeMake(10, 10) actualTime:NULL error:&error];
//    if (error == nil) {
//        UIImage *imgg_ = [[UIImage alloc] initWithCGImage:img];
//        UIImage *img_ = [ImageHelper image:imgg_ fillSize:CGSizeMake(80, 80)];
//        [imgg_ release];
//        NSString *namePic = [[contentName componentsSeparatedByString:@"."] objectAtIndex:0];
//        NSString *thumbnilstr = [namePic stringByAppendingFormat:@".png"];
//        filePathThumbnail = [kMOVIETHUMBPATH stringByAppendingPathComponent:thumbnilstr];
//        NSFileManager *fileManager = [NSFileManager defaultManager];
//        if (![fileManager fileExistsAtPath:kMOVIETHUMBPATH]) {
//            [fileManager createDirectoryAtPath:kMOVIETHUMBPATH withIntermediateDirectories:YES attributes:nil error:nil];
//        }
//        [UIImagePNGRepresentation(img_) writeToFile:filePathThumbnail atomically:YES];
//    }
//    CGImageRelease(img);
//    debug_NSLog(@"视频缩略图地址====%@",filePathThumbnail);
//    if (filePathThumbnail.length == 0)
//        {
//        debug_NSLog(@"视频缩略图地址====");
//        filePathThumbnail = @"";
//        }
//    return filePathThumbnail;
    
    return @"";
}

@end
