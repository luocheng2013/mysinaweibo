//
//  SlideEffectLabel.h
//  mCloud_iPhone
//
//  Created by user on 13-9-9.
//  Copyright (c) 2013年 epro. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef enum tagEffectDirection
{
    EffectDirectionLeftToRight,
    EffectDirectionRightToLeft,
    EffectDirectionTopToBottom,
    EffectDirectionBottomToTop,
    EffectDirectionTopLeftToBottomRight,
    EffectDirectionBottomRightToTopLeft,
    EffectDirectionBottomLeftToTopRight,
    EffectDirectionTopRightToBottomLeft
}
EffectDirection;


@interface SlideEffectLabel : UIView
{
    UILabel *_effectLabel;
    CGImageRef _alphaImage;
    CALayer *_textLayer;
}


@property (strong, nonatomic) UIFont *font;
@property (strong, nonatomic) UIColor *textColor;
@property (strong, nonatomic) UIColor *effectColor;
@property (strong, nonatomic) NSString *text;
@property (assign, nonatomic) EffectDirection effectDirection;


- (void)performEffectAnimation;
@end
