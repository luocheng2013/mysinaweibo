//
//  AppContextManagement.h
//  SinaWeiBo
//
//  Created by maowangxin on 14-3-6.
//  Copyright (c) 2014年 wangxin mao. All rights reserved.
//

#import <Foundation/Foundation.h>

@class HomeViewController;
@class MessageViewController;
@class NewViewController;
@class PlazaViewController;
@class UserViewController;
@class DDMenuController;
@class CustomTabBarVC;


@interface AppContextManagement : NSObject
{
    //全局屏幕快照数组
    NSMutableArray *screenShotArr;
    UIView *customRefreshBar;
}

@property (nonatomic, retain) NSMutableArray *screenShotArr;
@property (nonatomic, assign) int lastSelectNum;//上一次点击了那个Tabbar
@property (nonatomic, assign) int endSelectNum;//最后一次点击了那个Tabbar
@property (nonatomic, retain) UIView *customRefreshBar;
//全局tabbar类对象
@property (nonatomic, retain) DDMenuController *weiboDDMenuVC;
@property (nonatomic, retain) CustomTabBarVC *weiboTabbarVC;
@property (nonatomic, retain) HomeViewController *weiboHomeVC;
@property (nonatomic, retain) MessageViewController *weiboMessageVC;
@property (nonatomic, retain) NewViewController *weiboNewVC;
@property (nonatomic, retain) PlazaViewController *weiboPlazaVC;
@property (nonatomic, retain) UserViewController *weiboUserVC;
@property (nonatomic, assign) BOOL requestSuccess;

+ (id)shareAppContextmanagement;

- (void)createSinaWeiBoAllVC;

- (NSMutableArray *)createscreenShotArr;

- (UIView *)creatRefreshView:(NSString *)refreshNum;




-(void)clearUserData;
@end
