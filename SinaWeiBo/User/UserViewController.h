//
//  UserViewController.h
//  SinaWeiBo
//
//  Created by wangxin mao on 14-2-27.
//  Copyright (c) 2014年 wangxin mao. All rights reserved.
//

#import "BaseViewController.h"
#import "WeiboSDK.h"


@interface UserViewController : BaseViewController<UIAlertViewDelegate,WBHttpRequestDelegate>

@end
