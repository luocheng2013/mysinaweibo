//
//  UserViewController.m
//  SinaWeiBo
//
//  Created by wangxin mao on 14-2-27.
//  Copyright (c) 2014年 wangxin mao. All rights reserved.
//

#import "UserViewController.h"
#import "LoginViewController.h"


@interface UserViewController ()

@end

@implementation UserViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"我";

    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	self.view.backgroundColor = [UIColor groupTableViewBackgroundColor];

    UIImage *normalImage = [[UIImage imageNamed:@"common_button_red_os7"] stretchableImageWithLeftCapWidth:3 topCapHeight:3];
    UIButton *logoutBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [logoutBtn setTitle:@"退出当前帐号" forState:UIControlStateNormal];
    logoutBtn.frame = CGRectMake(9, 450, 301, 38);
    [logoutBtn setBackgroundImage:normalImage forState:UIControlStateNormal];
    [logoutBtn setBackgroundImage:normalImage forState:UIControlStateHighlighted];
    [logoutBtn addTarget:self action:@selector(loginOut) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:logoutBtn];
}

- (void)loginOut
{
    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"警告" message:@"您确定要退出当前帐号？" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    [alert show];
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        
        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
        
        LoginViewController *loginVC = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
        MLNavigationController *loginNav = [[MLNavigationController alloc] initWithRootViewController:loginVC];
        [self presentViewController:loginNav animated:NO completion:^{}];
        
        if ([userDefault objectForKey:@"WeiBo_Login_Token"]) {
            [WeiboSDK logOutWithToken:[userDefault objectForKey:@"WeiBo_Login_Token"] delegate:self withTag:@"logOutWithUserVC"];
        }
        
        AppContextManagement *app = [AppContextManagement shareAppContextmanagement];
        [app clearUserData];
        [userDefault setObject:nil forKey:@"WeiBo_Login_Token"];
        [userDefault synchronize];

    }
}

//注销回调方法
-(void)request:(WBHttpRequest*)request didReceiveResponse:(NSURLResponse *)response
{
    
}

- (void)request:(WBHttpRequest *)request didFinishLoadingWithResult:(NSString *)result
{
    [Common tipAlert:@"收到网络回调" tipStr:result];
}

- (void)request:(WBHttpRequest *)request didFailWithError:(NSError *)error;
{
    [Common tipAlert:@"请求异常" tipStr:[NSString stringWithFormat:@"%@",error]];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
