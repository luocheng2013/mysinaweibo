//
//  InfoCell.m
//  SinaWeiBo
//
//  Created by wangxin mao on 14-3-2.
//  Copyright (c) 2014年 wangxin mao. All rights reserved.
//

#import "InfoCell.h"


@implementation InfoCell


@synthesize weiboUserImg;
@synthesize weiboName;
@synthesize sendTime,changeWeibo;
@synthesize weiboContent;
@synthesize weiboBigImg;
@synthesize cellToolBarView;
@synthesize cellCotentView;


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.backgroundColor = [UIColor clearColor];
        UIView *selectBgView = [[UIView alloc] initWithFrame:self.frame];
        selectBgView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"common_card_center_background_highlighted"]];
        self.selectedBackgroundView = selectBgView;
        
        [self _intInfoCell_UI];
    }
    return self;
}

- (void)_intInfoCell_UI
{
    //所有的空间全部放在这个view上
    UIView *myCotentView = [[UIView alloc] initWithFrame:CGRectMake(5, 0, self.frame.size.width-10, 300)];
    myCotentView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    self.cellCotentView = myCotentView;
    [self addSubview:myCotentView];

    
    //微博博主头像
    UIImageView *userImg = [[UIImageView alloc] initWithFrame:CGRectMake(5, 5, 50, 50)];
    userImg.backgroundColor = [UIColor clearColor];
    [myCotentView addSubview:userImg];
    self.weiboUserImg = userImg;
    
    
    //微博名字
    UILabel *name = [[UILabel alloc] initWithFrame:CGRectMake(userImg.frame.size.width+20, 10, myCotentView.frame.size.width-userImg.frame.size.width-20-20, 20)];
    name.backgroundColor = [UIColor clearColor];
    name.textColor = [UIColor blackColor];
    name.font = [UIFont systemFontOfSize:14];
    [myCotentView addSubview:name];
    self.weiboName = name;
    
    
    //微博下拉收藏等功能
    UIButton *collectBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    collectBtn.frame = CGRectMake(name.frame.origin.x+name.frame.size.width-15, name.frame.origin.y-8, 40, 40);
    collectBtn.showsTouchWhenHighlighted = YES;
    collectBtn.backgroundColor = [UIColor clearColor];
    [collectBtn setImage:[UIImage imageNamed:@"timeline_icon_more"] forState:0];
    [collectBtn setTitleColor:[UIColor blackColor] forState:0];
    [collectBtn addTarget:self action:@selector(collectBtnBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [myCotentView addSubview:collectBtn];
    
    
    
    //发送时间
    UILabel *time = [[UILabel alloc] initWithFrame:CGRectMake(userImg.frame.size.width+20, name.frame.origin.y+name.frame.size.height+5, myCotentView.frame.size.width-userImg.frame.size.width-20, 20)];
    time.backgroundColor = [UIColor clearColor];
    time.textColor = [UIColor blackColor];
    time.font = [UIFont systemFontOfSize:14];
    [myCotentView addSubview:time];
    self.sendTime = time;
    
    
    //是否转发
    UILabel *change = [[UILabel alloc] initWithFrame:CGRectMake(userImg.frame.origin.x, time.frame.origin.y+time.frame.size.height+5, myCotentView.frame.size.width-5, 30)];
    change.backgroundColor = [UIColor clearColor];
    change.textColor = [UIColor blackColor];
    change.font = [UIFont systemFontOfSize:14];
    [myCotentView addSubview:change];
    self.changeWeibo = change;
//    DLog(@"是否转发的高度===%.f",time.frame.origin.y+time.frame.size.height+5+30);
    
    
    
    //内容
    UILabel *content = [[UILabel alloc] initWithFrame:CGRectMake(userImg.frame.origin.x, change.frame.origin.y+change.frame.size.height+5, myCotentView.frame.size.width-5, 20)];
    content.backgroundColor = [UIColor clearColor];
    content.textColor = [UIColor blackColor];
//    DLog(@"内容的高度===%.f",change.frame.origin.y+change.frame.size.height+15+20);
    content.font = [UIFont systemFontOfSize:14];
    [myCotentView addSubview:content];
    self.weiboContent = content;

    
    //内容图片
    UIImageView *bigImg = [[UIImageView alloc] initWithFrame:CGRectMake(userImg.frame.origin.x, content.frame.origin.y+content.frame.size.height+10, 100, 80)];
    bigImg.backgroundColor = [UIColor groupTableViewBackgroundColor];
    bigImg.image = [UIImage imageNamed:@"skin_loading_icon"];
    [myCotentView addSubview:bigImg];
    self.weiboBigImg = bigImg;
//    DLog(@"内容图片的高度===%.f",content.frame.origin.y+content.frame.size.height+10+80);
    
    
    //三个按钮背景
    UIView *toolBarView = [[UIView alloc] initWithFrame:CGRectMake(0, bigImg.frame.origin.y+bigImg.frame.size.height+10, myCotentView.frame.size.width, 40)];
    toolBarView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"more_Individuation_toolbar"]];
    self.cellToolBarView = toolBarView;
    [myCotentView addSubview:toolBarView];
    
    for (int j = 0; j<2; j++) {
        UILabel *lineLab = [[UILabel alloc] initWithFrame:CGRectMake(myCotentView.frame.size.width/3+(myCotentView.frame.size.width/3)*j+1, 12, 0.5, 15)];
        lineLab.backgroundColor = [UIColor grayColor];
        [toolBarView addSubview:lineLab];
    }
    
    //三个按钮
    NSArray *titleArr = [NSArray arrayWithObjects:@"转发",@"评论",@"赞", nil];
    NSArray *imgArr = [NSArray arrayWithObjects:@"toolbar_icon_retweet_os7",@"toolbar_icon_comment_os7",@"toolbar_icon_unlike_os7", nil];
    
    for (int i = 0 ; i < [titleArr count]; i++) {
        UIButton *toolBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        toolBtn.frame = CGRectMake((myCotentView.frame.size.width/3)*i, 0, myCotentView.frame.size.width/3, 40);
        toolBtn.backgroundColor = [UIColor clearColor];
        toolBtn.tag = i;
        [toolBtn setTitle:titleArr[i] forState:0];
        [toolBtn setImage:[UIImage imageNamed:imgArr[i]] forState:0];
        [toolBtn setTitleColor:[UIColor darkTextColor] forState:0];
        [toolBtn setTitleEdgeInsets:UIEdgeInsetsMake(20, 25, 20, 15)];
        toolBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        [toolBtn addTarget:self action:@selector(functionBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [toolBarView addSubview:toolBtn];
    }
    
    
    UILabel *lowLineLab = [[UILabel alloc] initWithFrame:CGRectMake(0, 37, myCotentView.frame.size.width, 0.5)];
    lowLineLab.backgroundColor = [UIColor grayColor];
    [toolBarView addSubview:lowLineLab];
}

//下拉菜单功能
- (void)functionBtnAction:(UIButton *)sender
{
    switch (sender.tag) {
        case 0:
            //转发逻辑
            break;
        case 1:
            //评论逻辑
            break;
        case 2:
            //赞逻辑
        default:
            break;
    }
}


- (void)setDataToCell:(NSMutableDictionary *)Dic contenTextHeight:(CGFloat)textHeight contentHeight:(CGFloat)height
{
    [self.cellCotentView setFrame:CGRectMake(5, 0, self.frame.size.width-10, height)];
    
    if ([Dic objectForKey:@"profile_image_url"]) {
        //异步请求图片
        [self getWeiboImgForAsync:[Dic objectForKey:@"profile_image_url"] setImgToImageView:self.weiboUserImg];
    }
    if ([Dic objectForKey:@"screen_name"]) {
        self.weiboName.text = [Dic objectForKey:@"screen_name"];
    }
    if ([Dic objectForKey:@"created_at"]) {
        self.sendTime.text = [Dic objectForKey:@"created_at"];
    }
    if ([Dic objectForKey:@"retweeted_status"]) {
        self.changeWeibo.text = @"转发微博";
    } else {
        self.changeWeibo.text = @"非转发微博";
    }
    
    
    if ([Dic objectForKey:@"text"]) {
        
        //计算有分几行显示
        self.weiboContent.hidden = NO;
        NSString *tempStr = [NSString stringWithFormat:@"%.f",textHeight/17];
        int num = [tempStr intValue];
        self.weiboContent.numberOfLines = num;
        self.weiboContent.text = [Dic objectForKey:@"text"];
        [self.weiboContent setFrame:CGRectMake(self.weiboUserImg.frame.origin.x, self.changeWeibo.frame.origin.y+self.changeWeibo.frame.size.height+10, self.cellCotentView.frame.size.width-5, textHeight)];
        //重新计算大图位置
        [self.weiboBigImg setFrame:CGRectMake(self.weiboContent.frame.origin.x, self.weiboContent.frame.origin.y+self.weiboContent.frame.size.height+10, 100, 80)];
        //重新计算评论按钮位置
        [self.cellToolBarView setFrame:CGRectMake(self.weiboBigImg.frame.origin.x, self.weiboBigImg.frame.origin.y+self.weiboBigImg.frame.size.height+10, self.cellCotentView.frame.size.width, 40)];

    } else {
        self.weiboContent.hidden = YES;
    }
    
    
    if ([Dic objectForKey:@"thumbnail_pic"]) {
        self.weiboBigImg.hidden = NO;
        [self.cellToolBarView setFrame:CGRectMake(0, self.weiboBigImg.frame.origin.y+self.weiboBigImg.frame.size.height+10, self.cellCotentView.frame.size.width, 40)];
        //异步请求图片
        [self getWeiboImgForAsync:[Dic objectForKey:@"thumbnail_pic"] setImgToImageView:self.weiboBigImg];
    } else {
        self.weiboBigImg.hidden = YES;
        [self.cellToolBarView setFrame:CGRectMake(0, self.weiboContent.frame.origin.y+self.weiboContent.frame.size.height+10, self.cellCotentView.frame.size.width, 40)];
    }
}

//异步请求网络图片
- (void)getWeiboImgForAsync:(NSString *)path setImgToImageView:(UIImageView *)imageView
{
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        //在另外的线程里面执行
        NSURL *addressURL = [NSURL URLWithString:path];
        NSData *imgData = [NSData dataWithContentsOfURL:addressURL];
        //对UI的操作必须放到主线程
        dispatch_sync(dispatch_get_main_queue(), ^{
            [imageView setImage:[UIImage imageWithData:imgData]];
        });
    });
}


- (void)collectBtnBtnAction:(UIButton *)sender
{
    AppContextManagement *app = [AppContextManagement shareAppContextmanagement];
    [app.weiboHomeVC collectActionFromCell];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
