//
//  PlazaCell.h
//  SinaWeiBo
//
//  Created by maowangxin on 14-3-18.
//  Copyright (c) 2014年 wangxin mao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlazaCell : UITableViewCell



@property (weak, nonatomic) IBOutlet UIImageView *customImage;
@property (weak, nonatomic) IBOutlet UILabel *customLab;

@end
