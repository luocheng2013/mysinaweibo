//
//  InfoCell.h
//  SinaWeiBo
//
//  Created by wangxin mao on 14-3-2.
//  Copyright (c) 2014年 wangxin mao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InfoCell : UITableViewCell


@property (nonatomic, retain) UIImageView *weiboUserImg;
@property (nonatomic, retain) UILabel *weiboName;
@property (nonatomic, retain) UILabel *sendTime;
@property (nonatomic, retain) UILabel *changeWeibo;
@property (nonatomic, retain) UILabel *weiboContent;
@property (nonatomic, retain) UIImageView *weiboBigImg;
@property (nonatomic, retain) UIView *cellToolBarView;
@property (nonatomic, retain) UIView *cellCotentView;


- (void)setDataToCell:(NSMutableDictionary *)Dic contenTextHeight:(CGFloat)textHeight contentHeight:(CGFloat)height;

@end
