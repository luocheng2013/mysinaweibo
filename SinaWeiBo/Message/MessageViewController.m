//
//  MessageViewController.m
//  SinaWeiBo
//
//  Created by wangxin mao on 14-2-27.
//  Copyright (c) 2014年 wangxin mao. All rights reserved.
//

#import "MessageViewController.h"
#import "DetailViewController.h"
#import "MessageCell.h"


@interface MessageViewController ()

@end

@implementation MessageViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"消息";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
        //右边按钮
    UIImage *rightBtnImg = [UIImage imageNamed:@"navigationbar_pop_os7"];
    UIButton *showRightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [showRightBtn setFrame:CGRectMake(0, 0, 30, 30)];
    [showRightBtn setImage:rightBtnImg forState:0];
    [showRightBtn addTarget:self action:@selector(showRightVCAction:) forControlEvents:UIControlEventTouchUpInside];
    showRightBtn.showsTouchWhenHighlighted = YES;
    UIBarButtonItem *showRightBtnItem = [[UIBarButtonItem alloc] initWithCustomView:showRightBtn];
    self.navigationItem.rightBarButtonItem = showRightBtnItem;
    
    
    
    UITableView *infoTab = [[UITableView alloc] initWithFrame:CGRectMake(0, 64, KscreenWidth, KscreenHeight) style:UITableViewStylePlain];
    infoTab.delegate = self;
    infoTab.dataSource = self;
    [infoTab setRowHeight:44];
    [self.view addSubview:infoTab];
    self.infoTableView = infoTab;
}

- (void)showRightVCAction:(id)sender
{
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 100;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    NSString *cellIdfaine = @"customCell";
    
    MessageCell *infoCell = [tableView dequeueReusableCellWithIdentifier:cellIdfaine];
    
    if (infoCell == nil) {
        infoCell = [[[NSBundle mainBundle] loadNibNamed:@"MessageCell" owner:self options:nil] lastObject];
    }
    return  infoCell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.infoTableView setFrame:CGRectMake(0, 64, KscreenWidth, KscreenHeight-20-44)];
    DetailViewController *detailVC = [[DetailViewController alloc] initWithNibName:@"DetailViewController" bundle:nil];
    [self.navigationController pushViewController:detailVC animated:YES];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.infoTableView setFrame:CGRectMake(0, 64, KscreenWidth, KscreenHeight-20-44-49)];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
        // Dispose of any resources that can be recreated.
}


@end
