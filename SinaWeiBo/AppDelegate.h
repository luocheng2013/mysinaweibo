//
//  AppDelegate.h
//  SinaWeiBo
//
//  Created by wangxin mao on 14-2-27.
//  Copyright (c) 2014年 wangxin mao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomTabBarVC.h"
#import "DDMenuController.h"
#import <AVFoundation/AVFoundation.h>
#import "CurtainView_iPhone.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    CurtainView_iPhone *curtainView;
    UIBackgroundTaskIdentifier bgTask;
}

@property (nonatomic, retain) CurtainView_iPhone *curtainView;
@property (strong, nonatomic) UIWindow *window;
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (nonatomic, retain) DDMenuController *ddMenuVC;
@property (nonatomic, retain) CustomTabBarVC *appTabBarVC;
@property (nonatomic, retain) AVAudioPlayer *myPplayer;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

@end
