//
//  NewViewController.m
//  SinaWeiBo
//
//  Created by wangxin mao on 14-2-27.
//  Copyright (c) 2014年 wangxin mao. All rights reserved.
//

#import "NewViewController.h"
#import "AMBlurView.h"
#import "SenderWeiboViewController.h"
#import "DRNRealTimeBlurView.h"


@interface NewViewController ()
{
    AppContextManagement *app;
    UIImageView *screenImgView;
//    AMBlurView *bigScreenView;
//    DRNRealTimeBlurView *bigScreenView;
    UIView *bigScreenView;
    UIButton *dissBtn;
    UIButton *testBtn;
    UIButton *testBtn1;
    UIButton *testBtn2;
    UIButton *testBtn3;
    UIButton *testBtn4;
    UIView *dragView;
    UIView *gestureView;
    UIImageView *backImgView;
    BOOL deagFlag;
    
}

@end

@implementation NewViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
         self.title = @"新建";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    app = [AppContextManagement shareAppContextmanagement];
    
    //系统win上的最上面一层视图，这里要把消失按钮、新建左右滑动的手势加在这层视图上
    UIWindow *tempWin = [[[UIApplication sharedApplication] windows] lastObject];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshSelfView:) name:@"AddNewWeiBoFromTwoTabbar" object:nil];
    
    
    //快照图片
    screenImgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@""]];
    [screenImgView setFrame:CGRectMake(0, 0, KscreenWidth, KscreenHeight)];
    
//    磨砂效果1
//    bigScreenView = [[AMBlurView alloc] initWithFrame:screenImgView.frame];
//    [bigScreenView setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight];
//    磨砂效果2
//    bigScreenView = [[DRNRealTimeBlurView alloc] initWithFrame:screenImgView.frame];
    bigScreenView = [[UIView alloc] initWithFrame:screenImgView.frame];
    [self.view addSubview:bigScreenView];

    
    //把半透明层加在win上
    [self.navigationController.view addSubview:screenImgView];
    
    [screenImgView addSubview:bigScreenView];
    screenImgView.hidden = YES;
    
    
    //滑动按钮层
    dragView = [[UIView alloc] initWithFrame:CGRectMake(0, 180, 320*2, 60)];
    dragView.backgroundColor = [UIColor clearColor];
    dragView.hidden = YES;
    
    
    
    testBtn1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [testBtn1 setTitle:@"发微博" forState:UIControlStateNormal];
    testBtn1.frame = CGRectMake(5, 5, 100, 50);
    UIImage *normalImage1 = [[UIImage imageNamed:@"common_button_big_green_os7"] stretchableImageWithLeftCapWidth:3 topCapHeight:3];
    [testBtn1 setBackgroundImage:normalImage1 forState:UIControlStateNormal];
    [testBtn1 addTarget:self action:@selector(senderWeiboVC:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    testBtn2 = [UIButton buttonWithType:UIButtonTypeCustom];
    [testBtn2 setTitle:@"相册" forState:UIControlStateNormal];
    testBtn2.frame = CGRectMake(110, 5, 100, 50);
    UIImage *normalImage2 = [[UIImage imageNamed:@"common_button_big_green_os7"] stretchableImageWithLeftCapWidth:3 topCapHeight:3];
    [testBtn2 setBackgroundImage:normalImage2 forState:UIControlStateNormal];
    [testBtn2 addTarget:self action:@selector(snederPhotoAction:) forControlEvents:UIControlEventTouchUpInside];

    
    
    testBtn3 = [UIButton buttonWithType:UIButtonTypeCustom];
    [testBtn3 setTitle:@"更多" forState:UIControlStateNormal];
    testBtn3.frame = CGRectMake(215, 5, 100, 50);
    UIImage *normalImage3 = [[UIImage imageNamed:@"common_button_big_green_os7"] stretchableImageWithLeftCapWidth:3 topCapHeight:3];
    [testBtn3 setBackgroundImage:normalImage3 forState:UIControlStateNormal];
    [testBtn3 addTarget:self action:@selector(moreTestBtn3:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    testBtn4 = [UIButton buttonWithType:UIButtonTypeCustom];
    [testBtn4 setTitle:@"测试4" forState:UIControlStateNormal];
    testBtn4.frame = CGRectMake(330, 5, 80, 50);
    UIImage *normalImage4 = [[UIImage imageNamed:@"common_button_big_green_os7"] stretchableImageWithLeftCapWidth:3 topCapHeight:3];
    [testBtn4 setBackgroundImage:normalImage4 forState:UIControlStateNormal];
    [testBtn4 addTarget:self action:@selector(dissWindowView) forControlEvents:UIControlEventTouchUpInside];

    
    [dragView addSubview:testBtn1];
    [dragView addSubview:testBtn2];
    [dragView addSubview:testBtn3];
    [dragView addSubview:testBtn4];
    
    
    //左右滑动手势层
    gestureView = [[UIView alloc] initWithFrame:self.view.bounds];
    gestureView.backgroundColor = [UIColor clearColor];
    [tempWin addSubview:gestureView];
    [tempWin addSubview:dragView];
    

    //向左滑动退出页面
    UISwipeGestureRecognizer *leftRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeFrom:)];
    leftRecognizer.direction = UISwipeGestureRecognizerDirectionLeft;
    [gestureView addGestureRecognizer:leftRecognizer];
    
    
    //向右滑动进入编辑页面
    UISwipeGestureRecognizer *rightRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeFrom:)];
    rightRecognizer.direction = UISwipeGestureRecognizerDirectionRight;
    [gestureView addGestureRecognizer:rightRecognizer];
    
    
    //消失半透明层手势.
    UITapGestureRecognizer *tapGesature = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dissWindowView)];
    [gestureView addGestureRecognizer:tapGesature];


    //消失按钮
    dissBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    dissBtn.backgroundColor = [UIColor whiteColor];
    dissBtn.frame = CGRectMake(0, KscreenHeight-49, KscreenWidth, 49);
    [dissBtn addTarget:self action:@selector(dissBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [tempWin addSubview:dissBtn];
    dissBtn.hidden = YES;
    backImgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"tabbar_compose_background_icon_close"]];
    [backImgView setFrame:CGRectMake(145, 12, 30, 30)];
    [dissBtn addSubview:backImgView];

}


- (void)moreTestBtn3:(UIButton *)sender
{
    [Common playSystemSound:@"composer_close"];//播放声音
    if (deagFlag) {
        [UIView animateWithDuration:0.5 animations:^{
            [dragView setFrame:CGRectMake(0, 180, 320*2, 60)];
            [backImgView setImage:[UIImage imageNamed:@"tabbar_compose_background_icon_close"]];
        } completion:^(BOOL finished) {
            deagFlag = NO;
        }];

    } else {
        [UIView animateWithDuration:0.5 animations:^{
            [dragView setFrame:CGRectMake(-320, 180, 320*2, 60)];
            [backImgView setImage:[UIImage imageNamed:@"navigationbar_back_highlighted"]];
        } completion:^(BOOL finished) {
            deagFlag = YES;
        }];
    }
}


- (void)dissBtnAction:(UIButton *)sender
{
    if (deagFlag) {
        [UIView animateWithDuration:0.5 animations:^{
            [dragView setFrame:CGRectMake(0, 180, 320*2, 60)];
            [backImgView setImage:[UIImage imageNamed:@"tabbar_compose_background_icon_close"]];
        } completion:^(BOOL finished) {
            deagFlag = NO;
        }];

    } else {
        [self dissWindowView];
    }
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[(CustomTabBarVC *)self.tabBarController appTabBar] setHidden:YES];
    [self.navigationController.navigationBar setHidden:YES];
    
//    在refreshSelfView方法里调用，切换tabbar时就有动画
    [self refreshUI];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[(CustomTabBarVC *)self.tabBarController appTabBar] setHidden:NO];
    [self.navigationController.navigationBar setHidden:NO];
    
    [self dissWindowView];
}

- (void)refreshSelfView:(NSNotification *)Notification
{
//    在viewWillAppear方法里调用，切换tabbar时就 没有 动画
//    [self refreshUI];
}


- (void)refreshUI
{
    UIImage *screenShortImg = nil;
    if ([app.screenShotArr count] > 0) {
        screenShortImg = [app.screenShotArr objectAtIndex:0];
    } else {
        screenShortImg = [UIImage imageNamed:@"fish.jpg"];
    }
    [screenImgView setImage:screenShortImg];
    
    dragView.hidden = NO;
    testBtn.hidden = NO;
    dissBtn.hidden = NO;
    screenImgView.hidden = NO;
    gestureView.hidden = NO;
}

- (void)dissWindowView
{
    [Common playSystemSound:@"composer_close"];//播放声音
    [backImgView setImage:[UIImage imageNamed:@"tabbar_compose_background_icon_close"]];
    [dragView setFrame:CGRectMake(0, 180, 320*2, 60)];
    dragView.hidden = YES;
    dissBtn.hidden = YES;
    testBtn.hidden = YES;
    screenImgView.hidden = YES;
    gestureView.hidden = YES;
    deagFlag = NO;
    app.weiboTabbarVC.selectedIndex = app.lastSelectNum;
    app.weiboTabbarVC.tabBarAnimationView.frame = CGRectMake((320/5)*app.lastSelectNum, 0, 320/5+2, 53);
    app.endSelectNum = app.lastSelectNum;
}

- (void)handleSwipeFrom:(UISwipeGestureRecognizer *)recognizer {
    //判断左右
    if (recognizer.direction == UISwipeGestureRecognizerDirectionRight) {
        [UIView animateWithDuration:0.5 animations:^{
            [dragView setFrame:CGRectMake(0, 180, 320*2, 60)];
            [backImgView setImage:[UIImage imageNamed:@"tabbar_compose_background_icon_close"]];
        } completion:^(BOOL finished) {
            deagFlag = NO;
        }];
        
    } else if (recognizer.direction == UISwipeGestureRecognizerDirectionLeft) {
        [UIView animateWithDuration:0.5 animations:^{
            [dragView setFrame:CGRectMake(-320, 180, 320*2, 60)];
            [backImgView setImage:[UIImage imageNamed:@"navigationbar_back_highlighted"]];
        } completion:^(BOOL finished) {
            deagFlag = YES;
        }];
    }
}


- (void)senderWeiboVC:(UIButton *)sender
{
    [self dissWindowView];
    SenderWeiboViewController *detailVC = [[SenderWeiboViewController alloc] init];
    MLNavigationController *detailNav = [[MLNavigationController alloc] initWithRootViewController:detailVC];
    [self presentViewController:detailNav animated:YES completion:^{}];
}

- (void)snederPhotoAction:(id)sender
{
//    [self dissWindowView];
    SenderWeiboViewController *detailVC = [[SenderWeiboViewController alloc] init];
    detailVC.senderType = @"snederPhotoAction";
//    UINavigationController *detailNav = [[UINavigationController alloc] initWithRootViewController:detailVC];
    [self.navigationController pushViewController:detailVC animated:YES];
//    [self presentViewController:detailNav animated:YES completion:^{    }];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
