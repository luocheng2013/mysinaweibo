//
//  DealWithRequestData.h
//  SinaWeiBo
//
//  Created by maowangxin on 14-3-14.
//  Copyright (c) 2014年 wangxin mao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WeiboSDK.h"


@interface DealWithRequestData : NSObject<WBHttpRequestDelegate>


- (id)initWithRequestTag:(NSString *)tag;

@end
