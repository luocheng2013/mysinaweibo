//
//  BaseViewController.m
//  SinaWeiBo
//
//  Created by wangxin mao on 14-2-27.
//  Copyright (c) 2014年 wangxin mao. All rights reserved.
//

#import "BaseViewController.h"
#import "DetailViewController.h"
#import "InfoCell.h"

@interface BaseViewController ()
{
    DDMenuController *ddmenu;
}

@end

@implementation BaseViewController



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	self.view.backgroundColor = [UIColor groupTableViewBackgroundColor];
    
    if (KsystemNum >= 7.0) {
        
        //避免IOS7中可以滑动的页面在最下方出现空白
        self.automaticallyAdjustsScrollViewInsets = NO;
        
        //打开图片选中页面时把导航栏标题颜色置为黑色,因为IOS7系统默认的导航栏颜色为白色,会导致标题看不清楚
        NSShadow *shadow = [[NSShadow alloc] init];
        shadow.shadowColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.8];
        shadow.shadowOffset = CGSizeMake(0, 1);
        
        //设置导航栏的颜色
//        [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navigationbar_background_os7"] forBarMetrics:UIBarMetricsDefault];
        [[UINavigationBar appearance] setBarTintColor:[UIColor whiteColor]];
        
        NSDictionary *appDic = [NSDictionary dictionaryWithObjectsAndKeys: [UIColor darkGrayColor], NSForegroundColorAttributeName,shadow, NSShadowAttributeName,[UIFont fontWithName:@"HelveticaNeue-CondensedBlack" size:21.0], NSFontAttributeName, nil];
        
        [[UINavigationBar appearance] setTitleTextAttributes:appDic];
    }
    
    ddmenu = [(AppDelegate *)[[UIApplication sharedApplication] delegate] ddMenuVC];
    
    //此手势是判断每个继承这个类的类，在类的self.view中向右滑动能推出ddmenu的左边抽屉
    if ([self.navigationController.viewControllers count] == 1) {
        UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(postPan:)];
        pan.delegate = (id<UIGestureRecognizerDelegate>)ddmenu;
        [self.view addGestureRecognizer:pan];
    }
}

- (void)postPan:(UIPanGestureRecognizer*)gesture {
    
//    NSLog(@"当前有多少个Nav=====%d",[self.navigationController.viewControllers count]);
    if ([self.navigationController.viewControllers count] == 1) {
        [ddmenu pan:gesture];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if ([self.navigationController.viewControllers count] > 1) {
        [(CustomTabBarVC *)self.tabBarController hideAppTabBar:YES];
    } else {
        [(CustomTabBarVC *)self.tabBarController hideAppTabBar:NO];
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
