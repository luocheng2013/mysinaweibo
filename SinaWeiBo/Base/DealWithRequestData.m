//
//  DealWithRequestData.m
//  SinaWeiBo
//
//  Created by maowangxin on 14-3-14.
//  Copyright (c) 2014年 wangxin mao. All rights reserved.
//

#import "DealWithRequestData.h"

@implementation DealWithRequestData

- (id)initWithRequestTag:(NSString *)tag
{
    self = [super init];
    if (self) {
        // Initialization code
    }
    return self;
}

#pragma mark - request delegate

- (void)request:(WBHttpRequest *)request didFinishLoadingWithDataResult:(NSData *)data
{
    NSError *error;
    //xcode自带解析类NSJSONSerialization从data中解析出数据放到字典中
    NSDictionary *weatherDic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&error];
    //DLog(@"解析出数据放到字典中===%@",weatherDic);
    [self dealWith_statuses:weatherDic statuses_tag:(NSString *)request.tag];
}

- (void)request:(WBHttpRequest *)request didReceiveResponse:(NSURLResponse *)response
{
    DLog(@"成功接受数据");
}

- (void)request:(WBHttpRequest *)request didFailWithError:(NSError *)error
{
    DLog(@"请求失败=%@",error);
    AppContextManagement *app = [AppContextManagement shareAppContextmanagement];
    if ([request.tag isEqualToString:@"statuses/home_timeline"]) {
        app.requestSuccess = NO;
    } else if ([request.tag isEqualToString:@"update.json"]) {
        UIAlertView *tipAlert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"发布微博失败！" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [tipAlert show];
    }
    
    [app.weiboHomeVC performSelector:@selector(doneLoadingTableViewData) withObject:nil afterDelay:1.0];
    [Common showAlertString:@"获取数据失败"];
}

- (void)request:(WBHttpRequest *)request didFinishLoadingWithResult:(NSString *)result
{
    AppContextManagement *app = [AppContextManagement shareAppContextmanagement];
    if ([request.tag isEqualToString:@"statuses/home_timeline"]) {
        app.requestSuccess = YES;
    }
    DLog(@"===didFinishLoadingWithResult===");
}

#pragma mark - 微博普通读取接口请求数据统一处理
- (void)dealWith_statuses:(NSDictionary *)receive_statusesDic statuses_tag:(NSString *)requestTag
{
    //得到普通读取接口解析后的数据
    NSMutableArray *statusesArr = nil;
    safe_get_objc_from_Dic_with_key(statusesArr, receive_statusesDic, @"statuses");
    
    //    DLog(@"statusesArr 里面的第一个对象是什么=====%@",[[statusesArr objectAtIndex:0] class])
    
    NSMutableArray *postDealWithArr = [[NSMutableArray alloc] initWithCapacity:10];
    
    if ([requestTag isEqualToString:@"users/show"]) {
        NSMutableDictionary *weiboDataDic = [[NSMutableDictionary alloc] initWithCapacity:10];
        if ([receive_statusesDic objectForKey:@"name"]) {
            [weiboDataDic setObject:[receive_statusesDic objectForKey:@"name"] forKey:@"screen_name"];
            [postDealWithArr addObject:weiboDataDic];
        }
    } else if ([requestTag isEqualToString:@"update.json"]) {
        UIAlertView *tipAlert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"发布微博成功！" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [tipAlert show];
    
    } else if ([requestTag isEqualToString:@"statuses/home_timeline"]){
        for (int i = 0; i < [statusesArr count]; i++) {
            
            NSMutableDictionary *weiboDataDic = [[NSMutableDictionary alloc] initWithCapacity:10];
                //获取的信息字典
            NSDictionary *oneDataDic = (NSDictionary *)[statusesArr objectAtIndex:i];
            
            NSDictionary *userDic = [oneDataDic objectForKey:@"user"];
                //微博头像
            NSString *userImgStr = [userDic objectForKey:@"profile_image_url"];
                //用户昵称
            NSString *userName = [userDic objectForKey:@"screen_name"];
                //创建时间
            NSString *createTime = [oneDataDic objectForKey:@"created_at"];
                //转发微博
            NSString *change = [oneDataDic objectForKey:@"retweeted_status"];
                //微博内容
            NSString *weiboText = [oneDataDic objectForKey:@"text"];
            
                //大图字典
            NSArray *bigImgArr = [oneDataDic objectForKey:@"pic_urls"];
                //微博第一张大图地址
            NSString *bigImgAddress;
            NSDictionary *bigImgDic;
            if ([bigImgArr count] > 0) {
                
                bigImgDic = (NSDictionary *)[bigImgArr objectAtIndex:0];
                if ([bigImgDic objectForKey:@"thumbnail_pic"]) {
                    bigImgAddress = [bigImgDic objectForKey:@"thumbnail_pic"];
                }
            }
            
            if (userImgStr) {
                [weiboDataDic setObject:userImgStr forKey:@"profile_image_url"];
            }
            if (userName) {
                [weiboDataDic setObject:userName forKey:@"screen_name"];
            }
            
            if (createTime) {
                [weiboDataDic setObject:createTime forKey:@"created_at"];
            }
            
            if (change) {
                [weiboDataDic setObject:change forKey:@"retweeted_status"];
            }
            if (weiboText) {
                [weiboDataDic setObject:weiboText forKey:@"text"];
            }
            
            if (bigImgAddress) {
                [weiboDataDic setObject:bigImgAddress forKey:@"thumbnail_pic"];
            }
            
            [postDealWithArr addObject:weiboDataDic];
        }
    }
    
    
    
    AppContextManagement *app = [AppContextManagement shareAppContextmanagement];
    
    [app.weiboHomeVC getWantToDealWithData:postDealWithArr statuses_tag:requestTag];
    
}

@end
