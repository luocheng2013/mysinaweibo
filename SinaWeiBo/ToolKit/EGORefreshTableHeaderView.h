//
//  EGORefreshTableHeaderView.h
//  Demo
//
//  Created by Devin Doty on 10/14/09October14.
//  Copyright 2009 enormego. All rights reserved.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "AppContextManagement.h"

@class AppContextManagement;

typedef enum{
	EGOOPullRefreshPulling = 0,
	EGOOPullRefreshNormal,
	EGOOPullRefreshLoading,
} EGOPullRefreshState;

//同步状态和时间对应的key
#define Synchronous [NSString stringWithFormat:@"LastSynchronous%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"APP_NUMBER"]]
//同步待办状态和时间对应的key
#define Synchronous_Todo [NSString stringWithFormat:@"LastSynchronous_Todo%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"APP_NUMBER"]]
//同步加密柜状态和时间对应的key
//#define Synchronous_EncryptionKey [NSString stringWithFormat:@"LastSynchronous_Encryption%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"APP_NUMBER"]]
#define Synchronous_Error [NSString stringWithFormat:@"LastSynchronous_Error%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"APP_NUMBER"]]

@protocol EGORefreshTableHeaderDelegate;
@interface EGORefreshTableHeaderView : UIView {
	
	id _delegate;
	EGOPullRefreshState _state;
    
	UILabel *_lastUpdatedLabel;
	UILabel *_statusLabel;
	CALayer *_arrowImage;
    UIImageView *_activityImageView;
    UIImageView *_bgImageView;
    AppContextManagement *appContent;
    
}
@property(nonatomic,assign) id <EGORefreshTableHeaderDelegate> delegate;
@property(nonatomic,readonly) UILabel *lastUpdateLabel;
@property(nonatomic,readonly) UILabel *statusLabel;
@property(nonatomic,readonly) CALayer *arrowImage;
@property(nonatomic,readonly) UIImageView *activityImageView;
@property(nonatomic,readonly) UIImageView *bgImageView;

- (id)initWithFrame:(CGRect)frame;
- (void)egoRefreshScrollViewDidScroll:(UIScrollView *)scrollView;
- (void)egoRefreshScrollViewDidEndDragging:(UIScrollView *)scrollView;
- (void)egoRefreshScrollViewDataSourceDidFinishedLoading:(UIScrollView *)scrollView;
- (void)refreshLastUpdatedDate:(NSString *)key;
- (void)customRefresh:(UIScrollView *)scrollView;

@end
@protocol EGORefreshTableHeaderDelegate
- (void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView*)view;
- (BOOL)egoRefreshTableHeaderDataSourceIsLoading:(EGORefreshTableHeaderView*)view;
@optional
- (NSDate*)egoRefreshTableHeaderDataSourceLastUpdated:(EGORefreshTableHeaderView*)view;
@end
