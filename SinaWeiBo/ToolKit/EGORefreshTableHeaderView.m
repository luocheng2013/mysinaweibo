//
//  EGORefreshTableHeaderView.m
//  Demo
//
//  Created by Devin Doty on 10/14/09October14.
//  Copyright 2009 enormego. All rights reserved.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

#import "EGORefreshTableHeaderView.h"
#import "AppContextManagement.h"

//#define TEXT_COLOR	 [UIColor colorWithRed:87.0/255.0 green:108.0/255.0 blue:137.0/255.0 alpha:1.0]
#define FLIP_ANIMATION_DURATION 0.18f


@interface EGORefreshTableHeaderView (Private)
- (void)setState:(EGOPullRefreshState)aState;
@end

@interface EGORefreshTableHeaderView ()
{
    int angle;
}
@end

@implementation EGORefreshTableHeaderView
@synthesize delegate=_delegate;
@synthesize arrowImage = _arrowImage;
@synthesize lastUpdateLabel = _lastUpdatedLabel;
@synthesize statusLabel = _statusLabel;
@synthesize activityImageView = _activityImageView;
@synthesize bgImageView = _bgImageView;


- (id)initWithFrame:(CGRect)frame
{    
    if (self = [super initWithFrame:frame]) {
        
		self.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        self.backgroundColor = [UIColor clearColor];
        
        appContent = [AppContextManagement shareAppContextmanagement];
        
		_lastUpdatedLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 10.0f, self.frame.size.width, 20.0f)];
		_lastUpdatedLabel.font = [UIFont systemFontOfSize:12.0f];
		_lastUpdatedLabel.textColor = [UIColor darkTextColor];
		_lastUpdatedLabel.backgroundColor = [UIColor clearColor];
		_lastUpdatedLabel.textAlignment = NSTextAlignmentCenter;
        _lastUpdatedLabel.text = @"最后更新:----/--/-- --:--";
		[self addSubview:_lastUpdatedLabel];
        
		
		_statusLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, -10.0f, self.frame.size.width, 20.0f)];
        _statusLabel.font = [UIFont systemFontOfSize:12.0f];
		_statusLabel.textColor = [UIColor darkTextColor];
		_statusLabel.backgroundColor = [UIColor clearColor];
		_statusLabel.textAlignment = NSTextAlignmentCenter;
		[self addSubview:_statusLabel];
		
        
		CALayer *layer = [CALayer layer];
		layer.frame = CGRectMake(30.0f, -5, 32, 32);
		layer.contentsGravity = kCAGravityResizeAspect;
		layer.contents = (id)[UIImage imageNamed:@"tableview_pull_refresh"].CGImage;
        
        
//        //下拉最下边蓝色线条
//        UILabel *lineLab = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 39 , self.frame.size.width, .5f)];
//        lineLab.backgroundColor = [UIColor blueColor];
//        [self addSubview:lineLab];
//        [lineLab release];
        
        
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 40000
		if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
			layer.contentsScale = [[UIScreen mainScreen] scale];
		}
#endif
		[[self layer] addSublayer:layer];
		_arrowImage=layer;
        
        
        //活动指示器gif图片
        UIImage* loadingImg = [UIImage imageNamed:@"navigationbar_icon_refresh_white_os7"];
        _activityImageView = [[UIImageView alloc] initWithImage:loadingImg];
        _activityImageView.frame = CGRectMake(30, -5.0f, 26, 26);
        [_activityImageView setHidden:YES];
        [self addSubview:_activityImageView];
        [self startAnimation];
        
        //下拉区各种状态
		[self setState:EGOOPullRefreshNormal];
    }
	
    return self;
}

-(void) startAnimation
{
//    [UIView beginAnimations:nil context:nil];
//    [UIView setAnimationDuration:0.01];
//    [UIView setAnimationDelegate:self];
//    [UIView setAnimationDidStopSelector:@selector(endAnimation)];
//    _activityImageView.transform = CGAffineTransformMakeRotation(angle * (M_PI / 180.0f));
//    [UIView commitAnimations];
}

-(void)endAnimation
{
//    angle += 10;
//    [self startAnimation];
}

#pragma mark Setters

- (void)setState:(EGOPullRefreshState)aState{
	
	switch (aState) {
		case EGOOPullRefreshPulling:
        
            [Common playSystemSound:@"prlm_sound_release"];//松开声音
			_statusLabel.text = @"释放更新";
            
			[CATransaction begin];
			[CATransaction setAnimationDuration:FLIP_ANIMATION_DURATION];
			_arrowImage.transform = CATransform3DMakeRotation((M_PI / 180.0) * 180.0f, 0.0f, 0.0f, 1.0f);
			[CATransaction commit];
			
			break;
		case EGOOPullRefreshNormal:
			
			if (_state == EGOOPullRefreshPulling) {
				[CATransaction begin];
				[CATransaction setAnimationDuration:FLIP_ANIMATION_DURATION];
				_arrowImage.transform = CATransform3DIdentity;
				[CATransaction commit];
			}
            
            [Common playSystemSound:@"prlm_sound_pull"];//下拉声音
            _statusLabel.text = @"下拉刷新";
            
            [_activityImageView setHidden:YES];
            
			[CATransaction begin];
			[CATransaction setValue:(id)kCFBooleanTrue forKey:kCATransactionDisableActions];
			_arrowImage.hidden = NO;
			_arrowImage.transform = CATransform3DIdentity;
			[CATransaction commit];
			
			break;
		case EGOOPullRefreshLoading:
			
            [Common playSystemSound:@"prlm_sound_triggering"];//触发声音
            _statusLabel.text = @"加载中...";
            
            [_activityImageView setHidden:NO];
			[CATransaction begin];
			[CATransaction setValue:(id)kCFBooleanTrue forKey:kCATransactionDisableActions];
			_arrowImage.hidden = YES;
			[CATransaction commit];
            
			break;
		default:
			break;
	}
	
	_state = aState;
}


#pragma mark ScrollView Methods

- (void)egoRefreshScrollViewDidScroll:(UIScrollView *)scrollView {
    
	if (_state == EGOOPullRefreshLoading) {
		CGFloat offset = MAX(scrollView.contentOffset.y * -1, 0);
		offset = MIN(offset, 60);
		scrollView.contentInset = UIEdgeInsetsMake(offset, 0.0f, 0.0f, 0.0f);
		
	} else if (scrollView.isDragging) {
		BOOL _loading = NO;
		if ([_delegate respondsToSelector:@selector(egoRefreshTableHeaderDataSourceIsLoading:)]) {
			_loading = [_delegate egoRefreshTableHeaderDataSourceIsLoading:self];
		}
		
		if (_state == EGOOPullRefreshPulling && scrollView.contentOffset.y > -65.0f && scrollView.contentOffset.y < 0.0f && !_loading) {
            
            [self setState:EGOOPullRefreshNormal];
            
		} else if (_state == EGOOPullRefreshNormal && scrollView.contentOffset.y < -65.0f && !_loading) {

            [self setState:EGOOPullRefreshPulling];
		}
		
		if (scrollView.contentInset.top != 0) {
			scrollView.contentInset = UIEdgeInsetsZero;
		}
	}
}

- (void)egoRefreshScrollViewDidEndDragging:(UIScrollView *)scrollView {
	
	BOOL _loading = NO;
	if ([_delegate respondsToSelector:@selector(egoRefreshTableHeaderDataSourceIsLoading:)]) {
		_loading = [_delegate egoRefreshTableHeaderDataSourceIsLoading:self];
	}
    
    if (scrollView.contentOffset.y <= - 65.0f && !_loading) {
        if ([_delegate respondsToSelector:@selector(egoRefreshTableHeaderDidTriggerRefresh:)]) {
            [_delegate egoRefreshTableHeaderDidTriggerRefresh:self];
        }
        [self setState:EGOOPullRefreshLoading];
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.2];
        scrollView.contentInset = UIEdgeInsetsMake(60.0f, 0.0f, 0.0f, 0.0f);
        [UIView commitAnimations];
    }
}

- (void)customRefresh:(UIScrollView *)scrollView
{
    if ([_delegate respondsToSelector:@selector(egoRefreshTableHeaderDidTriggerRefresh:)]) {
        [_delegate egoRefreshTableHeaderDidTriggerRefresh:self];
    }
    [self setState:EGOOPullRefreshLoading];
}

- (void)egoRefreshScrollViewDataSourceDidFinishedLoading:(UIScrollView *)scrollView {
    
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:.3];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(finishSynch)];
	[scrollView setContentInset:UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 0.0f)];
	[UIView commitAnimations];
    
    [self setState:EGOOPullRefreshNormal];
}


//更新完成时提示音、更新了几条微博
- (void)finishSynch
{
    if (appContent.requestSuccess) {
        [Common displayRefreshBar:appContent.weiboHomeVC.infoTableView refreshNum:[NSString stringWithFormat:@"更新了%d条微博",[appContent.weiboHomeVC.infoDataArr count]]];
        [Common playSystemSound:@"msgcome"];
    }
}

//刷新更新时间
- (void)refreshLastUpdatedDate:(NSString *)key
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    _lastUpdatedLabel.text = [NSString stringWithFormat:@"最后更新: %@", [formatter stringFromDate:[NSDate date]]];
}

#pragma mark Dealloc

- (void)dealloc {
    
	_delegate=nil;
	[_statusLabel release];
    _statusLabel = nil;
    [_arrowImage release];
	_arrowImage = nil;
    [_activityImageView release];
    _activityImageView = nil;
    [_lastUpdatedLabel release];
	_lastUpdatedLabel = nil;
    [super dealloc];
}


@end
