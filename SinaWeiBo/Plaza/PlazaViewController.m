//
//  PlazaViewController.m
//  SinaWeiBo
//
//  Created by wangxin mao on 14-2-27.
//  Copyright (c) 2014年 wangxin mao. All rights reserved.
//

#import "PlazaViewController.h"
#import "DetailViewController.h"
#import "PlazaCell.h"


@interface PlazaViewController ()

@end

@implementation PlazaViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"广场";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UITableView *infoTab = [[UITableView alloc] initWithFrame:CGRectMake(0, 64, KscreenWidth, KscreenHeight) style:UITableViewStylePlain];
    infoTab.delegate = self;
    infoTab.dataSource = self;
    [infoTab setRowHeight:44];
    [self.view addSubview:infoTab];
    self.infoTableView = infoTab;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 100;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    static NSString *cellIdfaine = @"PlazaCell";
    
    PlazaCell *infoCell = (PlazaCell *)[tableView dequeueReusableCellWithIdentifier:cellIdfaine];
    
    if (infoCell == nil) {
        
        infoCell = [[[NSBundle mainBundle] loadNibNamed:@"PlazaCell" owner:self options:nil] lastObject];
    }
    
    return  infoCell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.infoTableView setFrame:CGRectMake(0, 64, KscreenWidth, KscreenHeight-20-44)];
    DetailViewController *detailVC = [[DetailViewController alloc] initWithNibName:@"DetailViewController" bundle:nil];
    [self.navigationController pushViewController:detailVC animated:YES];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.infoTableView setFrame:CGRectMake(0, 64, KscreenWidth, KscreenHeight-20-44-49)];
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
