//
//  CustomTabBarVC.m
//  SinaWeiBo
//
//  Created by wangxin mao on 14-2-27.
//  Copyright (c) 2014年 wangxin mao. All rights reserved.
//

#import "CustomTabBarVC.h"

@interface CustomTabBarVC ()
{
    AppContextManagement *app;
}
@end

@implementation CustomTabBarVC

@synthesize appTabBar,appTabBarBtn,tabBarAnimationView;
@synthesize appHomeVC,appMessageVC,appNewVC,appPlazaVC,appUserVC;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.tabBar setHidden:YES];
	self.view.backgroundColor = [UIColor blackColor];
    app = [AppContextManagement shareAppContextmanagement];
    
    
    //主页
    HomeViewController *s_homeVC = [[HomeViewController alloc] init];
    MLNavigationController *homeNav = [[MLNavigationController alloc] initWithRootViewController:s_homeVC];
    self.appHomeVC = s_homeVC;
    
    
    //消息
    MessageViewController *s_messageVC = [[MessageViewController alloc] init];
    MLNavigationController *messageNav = [[MLNavigationController alloc] initWithRootViewController:s_messageVC];
    self.appMessageVC = s_messageVC;
    
    
    //新建
    NewViewController *s_newVC = [[NewViewController alloc] init];
    MLNavigationController *newNav = [[MLNavigationController alloc] initWithRootViewController:s_newVC];
    self.appNewVC = s_newVC;
    
    
    //广场
    PlazaViewController *s_plazaVC = [[PlazaViewController alloc] init];
    MLNavigationController *plazaNav = [[MLNavigationController alloc] initWithRootViewController:s_plazaVC];
    self.appPlazaVC = s_plazaVC;
    
    
    //用户
    UserViewController *s_userVC = [[UserViewController alloc] init];
    MLNavigationController *userNav = [[MLNavigationController alloc] initWithRootViewController:s_userVC];
    self.appUserVC = s_userVC;
    
    
    self.viewControllers = [NSArray arrayWithObjects:homeNav,messageNav,newNav,plazaNav,userNav, nil];
    
    //创建自定义的tabbar
    [self createTabBarButton];
    
}


- (void)createTabBarButton
{
    UIView *tabBarBgView = [[UIView alloc] initWithFrame:CGRectMake(0, KscreenHeight-49, KscreenWidth, 49)];
    tabBarBgView.tag = 10;
    self.appTabBar = tabBarBgView;
    tabBarBgView.backgroundColor = [UIColor whiteColor];//@"tabbar_background_os7"
    [self.view addSubview:tabBarBgView];
    
    //tabbar滑动块
    UIImageView *myAnimationView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320/5+2, 53)];
    [myAnimationView setImage:[UIImage imageNamed:@"tabbar_compose_button_os7"]];
    self.tabBarAnimationView = myAnimationView;
    [tabBarBgView addSubview:myAnimationView];
    
    //正常状态图片
    NSArray *tabBarNomalImgArr = [NSArray arrayWithObjects:@"tabbar_home",@"tabbar_message_center",@"tabbar_compose_background_icon_add",@"tabbar_discover",@"tabbar_profile", nil];
    
    //高亮状态图片 //新建icon   tabbar_compose_icon_add_os7
//    NSArray *tabBarSelectedImgArr = [NSArray arrayWithObjects:@"tabbar_home_highlighted",@"tabbar_message_center_highlighted",@"tabbar_compose_icon_add_highlighted",@"tabbar_discover_highlighted",@"tabbar_profile_highlighted", nil];
    
    //tabbar标题
    NSArray *tabbarTitleArr = [NSArray arrayWithObjects:@"首页",@"消息",@"新建",@"广场",@"我", nil];
    
    for (int i = 0; i<tabBarNomalImgArr.count; i++) {
        UIButton *tabBarButton = [[UIButton alloc] initWithFrame:CGRectMake((320/5)*i, 0, 320/5, 49)];
        tabBarButton.tag = i;
        [tabBarButton setImage:[UIImage imageNamed:tabBarNomalImgArr[i]] forState:UIControlStateNormal];
        [tabBarButton setImage:[UIImage imageNamed:tabBarNomalImgArr[i]] forState:UIControlStateHighlighted];
        [tabBarButton setImage:[UIImage imageNamed:tabBarNomalImgArr[i]] forState:UIControlStateSelected];
        [tabBarButton setTitle:tabbarTitleArr[i] forState:UIControlStateNormal];
        [tabBarButton setTitleColor:[UIColor darkTextColor] forState:UIControlStateNormal];
        [tabBarButton setTitleEdgeInsets:UIEdgeInsetsMake(40, -10, 10, 20)];
        [tabBarButton setImageEdgeInsets:UIEdgeInsetsMake(2, 16, 10, 10)];
        tabBarButton.titleLabel.font = [UIFont systemFontOfSize:11];
        [tabBarButton addTarget:self action:@selector(selectTabBarAction:) forControlEvents:UIControlEventTouchUpInside];
        self.appTabBarBtn = tabBarButton;
        [tabBarBgView addSubview:tabBarButton];
    }
}

- (void)selectTabBarAction:(UIButton *)sender
{
    if (sender.tag == 2) {
        [Common playSystemSound:@"composer_close"];//播放声音
    } else {
        app.lastSelectNum = sender.tag;
//        [Common playSystemSound:@"prlm_sound_triggering"];
    }
    
    if (sender.tag == app.endSelectNum) {
        
        //每次按了相同的tabbar后刷新相应的页面
        [self pressTwoTime:sender.tag];
        
        //抖动动画
        [UIView animateWithDuration:0.15 animations:^{
            [self.appTabBar setFrame:CGRectMake(0, KscreenHeight-39, KscreenWidth, 49)];

        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.1 animations:^{
                [self.appTabBar setFrame:CGRectMake(0, KscreenHeight-49, KscreenWidth, 49)];
            } completion:^(BOOL finished) {
            }];
        }];

    } else {
        app.endSelectNum = sender.tag;
        //Tabbar滑动动画
        self.tabBarAnimationView.hidden = NO;
        [UIView animateWithDuration:0.3 animations:^{
            self.tabBarAnimationView.center = sender.center;
        } completion:^(BOOL finished) {
            if (sender.tag == 2) {
                //滑动完成才出现新建页面
                [[NSNotificationCenter defaultCenter] postNotificationName:@"AddNewWeiBoFromTwoTabbar" object:nil];
            }
        }];
    }
    
    self.selectedIndex = sender.tag;
    
    
    //每次点击完下面的tabbar后截一张新建页面的磨砂背景图
    if (sender.tag != 2) {
        switch (sender.tag) {
            case 0:
            {
                [Common createMainScrenShot:self.appHomeVC.navigationController.view];
            }
                break;
            case 1:
            {
                [Common createMainScrenShot:self.appMessageVC.navigationController.view];
            }
                break;
            case 3:
            {
                [Common createMainScrenShot:self.appPlazaVC.navigationController.view];
            }
                break;
            case 4:
            {
                [Common createMainScrenShot:self.appUserVC.navigationController.view];
            }
                break;
                
            default:
                break;
        }
    }
}

//每次按了相同的tabbar
- (void)pressTwoTime:(NSInteger)selectNum
{
    switch (selectNum) {
        case 0:
        {
            //刷新主页页面
            [app.weiboHomeVC scrollTableView];
        }
            break;
        case 1:
        {
            //刷新消息页面
        }
            break;
        case 3:
        {
            //刷新新建页面
        }
            break;
        case 4:
        {
            //刷新广场页面
        }
            break;
            
        default:
            break;
    }
}

- (void)hideAppTabBar:(BOOL)animation
{
    if (animation) {
        [UIView animateWithDuration:0.3 animations:^{
            [self.appTabBar setFrame:CGRectMake(0, KscreenHeight, KscreenWidth, 49)];
        }];
    } else {
        [UIView animateWithDuration:0.15 animations:^{
            [self.appTabBar setFrame:CGRectMake(0, KscreenHeight-49, KscreenWidth, 49)];
        }];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
