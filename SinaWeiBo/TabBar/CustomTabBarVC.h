//
//  CustomTabBarVC.h
//  SinaWeiBo
//
//  Created by wangxin mao on 14-2-27.
//  Copyright (c) 2014年 wangxin mao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeViewController.h"
#import "MessageViewController.h"
#import "PlazaViewController.h"
#import "NewViewController.h"
#import "UserViewController.h"
#import "MLNavigationController.h"

@interface CustomTabBarVC : UITabBarController

@property (nonatomic, retain) UIView *appTabBar;
@property (nonatomic, retain) UIButton *appTabBarBtn;
@property (nonatomic, retain) UIImageView *tabBarAnimationView;

@property (nonatomic, retain) HomeViewController *appHomeVC;
@property (nonatomic, retain) MessageViewController *appMessageVC;
@property (nonatomic, retain) NewViewController *appNewVC;
@property (nonatomic, retain) PlazaViewController *appPlazaVC;
@property (nonatomic, retain) UserViewController *appUserVC;


- (void)hideAppTabBar:(BOOL)animation;
@end
