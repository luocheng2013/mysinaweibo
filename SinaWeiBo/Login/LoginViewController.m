//
//  LoginViewController.m
//  SinaWeiBo
//
//  Created by wangxin mao on 14-2-27.
//  Copyright (c) 2014年 wangxin mao. All rights reserved.
//

#import "LoginViewController.h"
#import "RegisterViewController.h"
#import "ForgetPassWordViewController.h"
#import "DetailViewController.h"

@interface LoginViewController ()

@end


@implementation LoginViewController

@synthesize wbtoken;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"登录";
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    //授权登录
    WBAuthorizeRequest *request = [WBAuthorizeRequest request];
    request.redirectURI = kRedirectURI;
    request.scope = @"all";
    request.userInfo = @{@"SSO_From": @"LoginViewController",
                         @"Other_Info_1": [NSNumber numberWithInt:123],
                         @"Other_Info_2": @[@"obj1", @"obj2"],
                         @"Other_Info_3": @{@"key1": @"obj1", @"key2": @"obj2"}};
    [WeiboSDK sendRequest:request];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    if ([userDefault objectForKey:@"WeiBo_Login_Token"]) {
        
        AppContextManagement *app = [AppContextManagement shareAppContextmanagement];
        CustomTabBarVC *tabBarVC = [(AppDelegate *)[[UIApplication sharedApplication] delegate] appTabBarVC];
        
        app.endSelectNum = 0;
        tabBarVC.selectedIndex = 0;
        tabBarVC.tabBarAnimationView.frame = CGRectMake(0, 0, 320/5+2, 53);
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor grayColor];
    
    UIImage *normalImage = [[UIImage imageNamed:@"common_button_big_green_os7"] stretchableImageWithLeftCapWidth:3 topCapHeight:3];
    [self.LoginBtn setBackgroundImage:normalImage forState:0];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.userNameTF resignFirstResponder];
    [self.passWordTF resignFirstResponder];
}


//输入帐号登录
- (IBAction)pressLoginAction:(id)sender {

    [self.userNameTF resignFirstResponder];
    [self.passWordTF resignFirstResponder];

    [self dismissViewControllerAnimated:YES completion:^{}];
}


#pragma mark - SinaWeibo delegate method
- (void)didReceiveWeiboRequest:(WBBaseRequest *)request
{
    if ([request isKindOfClass:WBProvideMessageForWeiboRequest.class]) {
        DLog(@"didReceiveWeiboRequest＝＝＝接收请求");
    }
}

- (void)didReceiveWeiboResponse:(WBBaseResponse *)response
{
    if ([response isKindOfClass:WBSendMessageToWeiboResponse.class]) {
        NSString *title = @"发送结果";
        NSString *message = [NSString stringWithFormat:@"响应状态: %d\n响应UserInfo数据: %@\n原请求UserInfo数据: %@",(int)response.statusCode, response.userInfo, response.requestUserInfo];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                        message:message
                                                       delegate:nil
                                              cancelButtonTitle:@"确定"
                                              otherButtonTitles:nil];
        [alert show];
        
    } else if ([response isKindOfClass:WBAuthorizeResponse.class]) {
        
        NSString *message = [NSString stringWithFormat:@"响应状态: %d\nresponse.userId: %@\nresponse.accessToken: %@\n响应UserInfo数据: %@\n原请求UserInfo数据: %@",(int)response.statusCode,[(WBAuthorizeResponse *)response userID], [(WBAuthorizeResponse *)response accessToken], response.userInfo, response.requestUserInfo];
        
        DLog(@"认证结果回调的信息=======%@",message);
        
        self.wbtoken = [(WBAuthorizeResponse *)response accessToken];
        
        DLog(@"登录获取的token======%@",self.wbtoken);
    }

    NSString *tipStr = nil;
    if (response.statusCode == 0) {
        
        tipStr = @"登录成功";
        [[NSUserDefaults standardUserDefaults] setObject:self.wbtoken forKey:@"WeiBo_Login_Token"];
        [[NSUserDefaults standardUserDefaults] synchronize];

        [self dismissViewControllerAnimated:YES completion:^{ }];
        
    } else {
        tipStr = @"登录失败";
    }
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"认证结果"
                                                    message:tipStr
                                                   delegate:nil
                                          cancelButtonTitle:@"确定"
                                          otherButtonTitles:nil];
    [alert show];
    
}



- (void)request:(WBHttpRequest *)request didFinishLoadingWithResult:(NSString *)result
{
    NSString *title = nil;
    UIAlertView *alert = nil;
    
    title = @"收到网络回调";
    alert = [[UIAlertView alloc] initWithTitle:title
                                       message:[NSString stringWithFormat:@"%@",result]
                                      delegate:nil
                             cancelButtonTitle:@"确定"
                             otherButtonTitles:nil];
    [alert show];
}


- (void)request:(WBHttpRequest *)request didFailWithError:(NSError *)error;
{
    NSString *title = nil;
    UIAlertView *alert = nil;
    
    title = @"请求异常";
    alert = [[UIAlertView alloc] initWithTitle:title
                                       message:[NSString stringWithFormat:@"%@",error]
                                      delegate:nil
                             cancelButtonTitle:@"确定"
                             otherButtonTitles:nil];
    [alert show];
}


//注册
- (IBAction)RegisterAction:(id)sender {
    RegisterViewController *registerVC = [[RegisterViewController alloc] initWithNibName:@"RegisterViewController" bundle:nil];
    [self.navigationController pushViewController:registerVC animated:YES];
}


//忘记密码
- (IBAction)forgetPAsswordAction:(id)sender {
    ForgetPassWordViewController *forgetPasswordVC = [[ForgetPassWordViewController alloc] initWithNibName:@"ForgetPassWordViewController" bundle:nil];
    [self.navigationController pushViewController:forgetPasswordVC animated:YES];
}

@end
