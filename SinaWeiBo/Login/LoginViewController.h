//
//  LoginViewController.h
//  SinaWeiBo
//
//  Created by wangxin mao on 14-2-27.
//  Copyright (c) 2014年 wangxin mao. All rights reserved.
//

#import "BaseViewController.h"

@interface LoginViewController : BaseViewController<UITextFieldDelegate>


@property (strong, nonatomic) NSString *wbtoken;
@property (weak, nonatomic) IBOutlet UITextField *userNameTF;
@property (weak, nonatomic) IBOutlet UITextField *passWordTF;
@property (weak, nonatomic) IBOutlet UIButton *loginOutBtn;
@property (weak, nonatomic) IBOutlet UIButton *forgetPassword;
@property (weak, nonatomic) IBOutlet UIButton *LoginBtn;


- (IBAction)pressLoginAction:(id)sender;
- (IBAction)RegisterAction:(id)sender;
- (IBAction)forgetPAsswordAction:(id)sender;



@end
